#include "Engine.h"

int main()
{
	Engine::Initialize("Island", 1024, 768, false);

	Camera camera = Camera(0, 0, 0);
	Engine::SetCamera(&camera);

	while (Engine::Run())
	{
		Engine::Clear();
		Engine::Present();
	}

	Engine::Deinitialize();
	return 0;
}