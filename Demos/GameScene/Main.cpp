#include "Engine.h"
#include "Modules/Freelook.h"
#include "Modules/Skybox.h"
#include "Modules/Terrain.h"

int main()
{
	float planeVertices[12] =
	{
		-0.5, 0, 0.5,
		0.5, 0, 0.5,
		-0.5, 0, -0.5,
		0.5, 0, -0.5
	};

	unsigned int planeIndices[6] =
	{
		0, 1, 2,
		1, 3, 2
	};

	float waterPlaneColor[12] =
	{
		0.25, 0.75, 1,
		0.25, 0.75, 1,
		0.25, 0.75, 1,
		0.25, 0.75, 1
	};

	float waterPlaneColor2[12] =
	{
		0.1, 0.5, 0.75,
		0.1, 0.5, 0.75,
		0.1, 0.5, 0.75,
		0.1, 0.5, 0.75
	};

	Engine::Initialize("Game Scene", 1280, 720, false);

	Freelook mainCamera = Freelook(0.1f, 1000, 5.0f);
	mainCamera.SetNearClip(10);
	mainCamera.SetFarClip(20000);
	mainCamera.SetColor(0.5, 0.85, 1);
	Engine::SetCamera(&mainCamera);

	Material standardMaterial = Material("../../Resources/Shaders/StandardColor.hlsl");

	Scene scene;

	Model water;
	water.SetVertices(planeVertices, 4);
	water.SetIndices(planeIndices, 6);
	water.SetColors(waterPlaneColor, 4);
	water.SetGeneratedNormals(planeVertices, 4, planeIndices, 6);
	water.SetMaterial(&standardMaterial);
	water.SetScale(8000, 1, 4000);
	water.SetPosition(0, -900, 0);
	scene.Add(&water);

	Model grid;
	grid.SetVertices(planeVertices, 4);
	grid.SetIndices(planeIndices, 6);
	grid.SetColors(waterPlaneColor2, 4);
	grid.SetGeneratedNormals(planeVertices, 4, planeIndices, 6);
	grid.SetMaterial(&standardMaterial);
	grid.SetInstanceCount(13 * 22);
	for (size_t x = 0; x < 22; x++)
	{
		for (size_t z = 0; z < 13; z++)
		{
			if (x < 11)
			{
				grid.SetPosition(x * 100.0f - 1100, -899, -600 + z * 100.0f, x * 13 + z);
			}
			else
			{
				grid.SetPosition(x * 100.0f - 1000, -899, -600 + z * 100.0f, x * 13 + z);
			}

			grid.SetScale(95, 95, 95, x * 13 + z);
		}
	}
	scene.Add(&grid);

	Model sand = Model("../../Resources/GameScene/Sand_model.obj", true);
	sand.SetMaterial(&standardMaterial);
	scene.Add(&sand);

	Model palm = Model("../../Resources/GameScene/TreePalm2_model.obj", true);
	palm.SetMaterial(&standardMaterial);
	palm.SetInstanceCount(30);
	palm.SetPosition(2000, -800, 0, 0);
	palm.SetPosition(2000, -750, 250, 1);
	palm.SetPosition(2000, -750, -250, 2);
	palm.SetPosition(2000, -700, 500, 3);
	palm.SetPosition(2000, -700, -500, 4);
	palm.SetPosition(2000, -650, 750, 5);
	palm.SetPosition(2000, -650, -750, 6);
	palm.SetPosition(2500, -500, 100, 7);
	palm.SetPosition(2500, -500, -100, 8);
	palm.SetPosition(2500, -450, 400, 9);
	palm.SetPosition(2500, -450, -400, 10);
	palm.SetPosition(2500, -450, 700, 11);
	palm.SetPosition(2500, -450, -700, 12);
	palm.SetPosition(2500, -400, 1000, 13);
	palm.SetPosition(2500, -400, -1000, 14);
	palm.SetPosition(-2000, -800, 0, 15);
	palm.SetPosition(-2000, -750, 250, 16);
	palm.SetPosition(-2000, -750, -250, 17);
	palm.SetPosition(-2000, -700, 500, 18);
	palm.SetPosition(-2000, -700, -500, 19);
	palm.SetPosition(-2000, -650, 750, 20);
	palm.SetPosition(-2000, -650, -750, 21);
	palm.SetPosition(-2500, -500, 100, 22);
	palm.SetPosition(-2500, -500, -100, 23);
	palm.SetPosition(-2500, -450, 400, 24);
	palm.SetPosition(-2500, -450, -400, 25);
	palm.SetPosition(-2500, -450, 700, 26);
	palm.SetPosition(-2500, -450, -700, 27);
	palm.SetPosition(-2500, -400, 1000, 28);
	palm.SetPosition(-2500, -400, -1000, 29);
	for (size_t i = 0; i < 30; i++)
	{
		palm.SetRotation(0, 420 * i , 0, i);
	}
	scene.Add(&palm);

	Model cactus = Model("../../Resources/GameScene/Cactus1_model.obj", true);
	cactus.SetMaterial(&standardMaterial);
	cactus.SetInstanceCount(6);
	cactus.SetPosition(-1500, -450, -1800, 0);
	cactus.SetPosition(800, -400, 2000, 1);
	cactus.SetPosition(-200, -425, -1900, 2);
	cactus.SetPosition(500, -350, 2400, 3);
	cactus.SetPosition(-1300, -500, 1600, 4);
	cactus.SetPosition(-1600, -350, -2600, 5);
	scene.Add(&cactus);

	Model fence = Model("../../Resources/GameScene/waterFence_model.obj", true);
	fence.SetMaterial(&standardMaterial);
	fence.SetPosition(0, -900, 0);
	scene.Add(&fence);

	Model rock1 = Model("../../Resources/GameScene/Rock_model.obj", true);
	rock1.SetMaterial(&standardMaterial);
	rock1.SetInstanceCount(10);
	rock1.SetPosition(100, -900, -600, 0);
	rock1.SetPosition(700, -900, -400, 1);
	rock1.SetPosition(-500, -900, -300, 2);
	rock1.SetPosition(-1000, -900, -200, 3);
	rock1.SetPosition(400, -900, -100, 4);
	rock1.SetPosition(300, -900, 0, 5);
	rock1.SetPosition(900, -900, 100, 6);
	rock1.SetPosition(-700, -900, 200, 7);
	rock1.SetPosition(-800, -900, 400, 8);
	rock1.SetPosition(-200, -900, 500, 9);
	scene.Add(&rock1);

	Model rock2 = Model("../../Resources/GameScene/Rock1Model.obj", true);
	rock2.SetMaterial(&standardMaterial);
	rock2.SetInstanceCount(10);
	rock2.SetPosition(-100, -900, -600, 0);
	rock2.SetPosition(-700, -900, 400, 1);
	rock2.SetPosition(500, -900, 300, 2);
	rock2.SetPosition(1000, -900, 200, 3);
	rock2.SetPosition(-400, -900, 100, 4);
	rock2.SetPosition(-300, -900, 600, 5);
	rock2.SetPosition(-900, -900, -100, 6);
	rock2.SetPosition(700, -900, -200, 7);
	rock2.SetPosition(800, -900, -400, 8);
	rock2.SetPosition(200, -900, 0, 9);
	for (size_t i = 0; i < 10; i++)
	{
		rock2.SetScale(2, 4, 4, i);
	}
	scene.Add(&rock2);

	Model balista = Model("../../Resources/GameScene/Balista_model.obj", true);
	balista.SetMaterial(&standardMaterial);
	balista.SetPosition(-800, -890, 200);
	scene.Add(&balista);

	Model bank = Model("../../Resources/GameScene/bankTower_model.obj", true);
	bank.SetMaterial(&standardMaterial);
	bank.SetPosition(-800, -890, -100);
	scene.Add(&bank);

	Model catapult = Model("../../Resources/GameScene/Catapult_model.obj", true);
	catapult.SetMaterial(&standardMaterial);
	catapult.SetPosition(-700, -890, -200);
	scene.Add(&catapult);

	Model eletricalTower = Model("../../Resources/GameScene/ElectricalTower_model.obj", true);
	eletricalTower.SetMaterial(&standardMaterial);
	eletricalTower.SetPosition(-600, -890, -300);
	scene.Add(&eletricalTower);

	Model flameThrower = Model("../../Resources/GameScene/FlameThrower_model.obj", true);
	flameThrower.SetMaterial(&standardMaterial);
	flameThrower.SetPosition(-200, -890, 100);
	scene.Add(&flameThrower);

	Model harpoon = Model("../../Resources/GameScene/Harpoon_model.obj", true);
	harpoon.SetMaterial(&standardMaterial);
	harpoon.SetPosition(-600, -890, 300);
	scene.Add(&harpoon);

	Model paperFactory = Model("../../Resources/GameScene/paperFactory_model.obj", true);
	paperFactory.SetMaterial(&standardMaterial);
	paperFactory.SetPosition(-500, -890, 100);
	scene.Add(&paperFactory);

	Model oilTower = Model("../../Resources/GameScene/OilTower_model.obj", true);
	oilTower.SetMaterial(&standardMaterial);
	oilTower.SetPosition(800, -890, -100);
	scene.Add(&oilTower);

	Model slingshot = Model("../../Resources/GameScene/SlingShot_model.obj", true);
	slingshot.SetMaterial(&standardMaterial);
	slingshot.SetPosition(500, -890, 200);
	scene.Add(&slingshot);

	Model snowTower = Model("../../Resources/GameScene/SnowTower_model.obj", true);
	snowTower.SetMaterial(&standardMaterial);
	snowTower.SetPosition(600, -890, -300);
	scene.Add(&snowTower);

	Model snowTower2 = Model("../../Resources/GameScene/SnowTower2_model.obj", true);
	snowTower2.SetMaterial(&standardMaterial);
	snowTower2.SetPosition(700, -890, 400);
	scene.Add(&snowTower2);

	Model sprinkler = Model("../../Resources/GameScene/Sprinkler_model.obj", true);
	sprinkler.SetMaterial(&standardMaterial);
	sprinkler.SetPosition(200, -890, 500);
	scene.Add(&sprinkler);

	Model treeTower = Model("../../Resources/GameScene/Tree1Tower_model.obj", true);
	treeTower.SetMaterial(&standardMaterial);
	treeTower.SetPosition(300, -890, -600);
	scene.Add(&treeTower);

	Model waterTower = Model("../../Resources/GameScene/WaterTower_model.obj", true);
	waterTower.SetMaterial(&standardMaterial);
	waterTower.SetPosition(100, -890, 600);
	scene.Add(&waterTower);

	Model boat = Model("../../Resources/GameScene/Boat0_model.obj", true);
	boat.SetMaterial(&standardMaterial);
	boat.SetInstanceCount(3);
	boat.SetPosition(-1100, -900, 0, 0);
	boat.SetPosition(-800, -900, 0, 1);
	boat.SetPosition(-600, -900, 0, 2);
	scene.Add(&boat);

	Model boat1 = Model("../../Resources/GameScene/Boat1_model.obj", true);
	boat1.SetMaterial(&standardMaterial);
	boat1.SetInstanceCount(3);
	boat1.SetPosition(-500, -900, -100, 0);
	boat1.SetPosition(-400, -900, 0, 1);
	boat1.SetPosition(-700, -900, 100, 2);
	scene.Add(&boat1);

	Model boat2 = Model("../../Resources/GameScene/Boat2_model.obj", true);
	boat2.SetMaterial(&standardMaterial);
	boat2.SetInstanceCount(3);
	boat2.SetPosition(-600, -900, 200, 0);
	boat2.SetPosition(-300, -900, 300, 1);
	boat2.SetPosition(-500, -900, -200, 2);
	scene.Add(&boat2);
	
	Model plane = Model("../../Resources/GameScene/Plane0_model.obj", true);
	plane.SetMaterial(&standardMaterial);
	plane.SetInstanceCount(3);
	plane.SetPosition(1100, -875, 0, 0);
	plane.SetPosition(800, -875, 0, 1);
	plane.SetPosition(600, -875, 0, 2);
	scene.Add(&plane);

	Model plane1 = Model("../../Resources/GameScene/Plane1_model.obj", true);
	plane1.SetMaterial(&standardMaterial);
	plane1.SetInstanceCount(3);
	plane1.SetPosition(500, -875, -100, 0);
	plane1.SetPosition(400, -875, 0, 1);
	plane1.SetPosition(700, -875, 100, 2);
	scene.Add(&plane1);

	Model plane2 = Model("../../Resources/GameScene/Plane2_model.obj", true);
	plane2.SetMaterial(&standardMaterial);
	plane2.SetInstanceCount(3);
	plane2.SetPosition(600, -875, 200, 0);
	plane2.SetPosition(300, -875, 300, 1);
	plane2.SetPosition(500, -875, -200, 2);
	scene.Add(&plane2);

	for (size_t i = 0; i < 3; i++)
	{
		boat.SetRotation(0, 90, 0, i);
		boat1.SetRotation(0, 90, 0, i);
		boat2.SetRotation(0, 90, 0, i);

		plane.SetRotation(0, -90, 0, i);
		plane1.SetRotation(0, -90, 0, i);
		plane2.SetRotation(0, -90, 0, i);
	}

	Light sun = Light::CreateDirectionalLight(50, -45, 0, 1, 1, 1, 1.25);
	Light ambient1 = Light::CreateDirectionalLight(0, 45, 0, 1, 1, 1, 0.75);
	Light ambient2 = Light::CreateDirectionalLight(0, 225, 0, 1, 1, 1, 0.75);

	Engine::SetScene(&scene);

	while (Engine::Run())
	{
		mainCamera.Update();

		Engine::Render();
	}

	Engine::Deinitialize();
	return 0;
}