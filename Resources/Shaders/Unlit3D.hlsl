cbuffer TransformBuffer : register(b12)
{
	matrix transform[1024];
}

cbuffer EngineBuffer : register(b13)
{
	matrix view;
	matrix projection;
};

Texture2D mainTexture : register(t0);
SamplerState mainSampler : register(s0);

struct VertexInput
{
	float4 position : POSITION;
	float2 tex : TEXCOORD;
};

struct PixelInput
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD;
};

PixelInput mainVS(VertexInput input, uint id : SV_InstanceID)
{
	PixelInput output;
	
	output.position = mul(input.position, transform[id]);
	output.position = mul(output.position, view);
	output.position = mul(output.position, projection);
	
	output.tex = input.tex;
	
	return output;
}

float4 mainPS(PixelInput input) : SV_TARGET
{
	return mainTexture.Sample(mainSampler, input.tex);
}