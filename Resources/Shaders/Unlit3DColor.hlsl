cbuffer TransformBuffer : register(b12)
{
	matrix transform[1024];
}

cbuffer EngineBuffer : register(b13)
{
	matrix view;
	matrix projection;
};

struct VertexInput
{
	float4 position : POSITION;
	float3 color : COLOR;
};

struct PixelInput
{
	float4 position : SV_POSITION;
	float3 color : COLOR;
};

PixelInput mainVS(VertexInput input, uint id : SV_InstanceID)
{
	PixelInput output;
	
	output.position = mul(input.position, transform[id]);
	output.position = mul(output.position, view);
	output.position = mul(output.position, projection);
	
	output.color = input.color;
	
	return output;
}

float4 mainPS(PixelInput input) : SV_TARGET
{
	return float4(input.color, 1);
}