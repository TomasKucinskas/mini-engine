cbuffer EngineBuffer : register(b13)
{
	matrix view;
	matrix projection;
};

Texture2D mainTexture : register(t0);
SamplerState mainSampler : register(s0);

struct VertexInput
{
	float4 position : POSITION;
	float2 tex : TEXCOORD;
};

struct PixelInput
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD;
};

PixelInput mainVS(VertexInput input)
{
	PixelInput output;

	matrix normalizedView = view;
	normalizedView._m30 = normalizedView._m31 = normalizedView._m32 = 0;

	output.position = mul(input.position, normalizedView);
	output.position = mul(output.position, projection);
	
	output.tex = input.tex;
	
	return output;
}

float4 mainPS(PixelInput input) : SV_TARGET
{
	return mainTexture.Sample(mainSampler, input.tex);
}