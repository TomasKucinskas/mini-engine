Texture2D mainTexture : register(t0);
SamplerState mainSampler : register(s0);

struct VertexInput
{
	float4 position : POSITION;
	float2 tex : TEXCOORD;
};

struct PixelInput
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD;
};

PixelInput mainVS(VertexInput input)
{
	PixelInput output;
	
	output.position = input.position;
	output.tex = input.tex;
	
	return output;
}

float4 mainPS(PixelInput input) : SV_TARGET
{
	return mainTexture.Sample(mainSampler, input.tex);
}