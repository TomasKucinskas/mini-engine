struct Light
{
	matrix view;
	matrix projection;

	float3 position;
	float range;
	
	float3 color;
	float intensity;
	
	float3 direction;
	float angle;
	
	int type;
};

cbuffer TransformBuffer : register(b12)
{
	matrix transform[1024];
}

cbuffer EngineBuffer : register(b13)
{
	matrix view;
	matrix projection;
	int lightCount;
	
	Light lights[8];
};

Texture2D depthMaps[8] : register(t16);
SamplerState mainSampler : register(s0);

struct VertexInput
{
	float4 position : POSITION;
	float3 normal : NORMAL;
	float4 color : COLOR;
};

struct PixelInput
{
	float4 position : SV_POSITION;
	float3 worldPosition : POSITION0;
	float3 normal : NORMAL;
	float4 color : COLOR;
};

PixelInput mainVS(VertexInput input, uint id : SV_InstanceID)
{
	PixelInput output;
	
	output.position = mul(input.position, transform[id]);
	output.worldPosition = output.position.xyz;
	
	output.position = mul(output.position, view);
	output.position = mul(output.position, projection);
	
	matrix rotation = transform[id];
	rotation._m30 = rotation._m31 = rotation._m32 = 0;
	
	output.normal = normalize(mul(input.normal, rotation));
	output.color = input.color;
	
	return output;
}

float4 mainPS(PixelInput input) : SV_TARGET
{
	float3 light = float3(0, 0, 0);
	
	for(int i = 0; i < lightCount; i++)
	{	
		switch(lights[i].type)
		{
		case 0:		// Directional light
			light += lights[i].color * lights[i].intensity
				* saturate(dot(input.normal, normalize(lights[i].direction)));
			break;
			
		case 1:		// Point light
			light += lights[i].color * lights[i].intensity
				* saturate(1 - sqrt(length(lights[i].position - input.worldPosition) / lights[i].range))
				* saturate(dot(input.normal, normalize(lights[i].position - input.worldPosition)));
			break;
			
		case 2:		// Spot light		
			float3 lightOutput = lights[i].color * lights[i].intensity
				* saturate(1 - sqrt(length(lights[i].position - input.worldPosition) / lights[i].range))
				* dot(input.normal, normalize(lights[i].position - input.worldPosition))
				* saturate((dot(normalize(input.worldPosition - lights[i].position), lights[i].direction) - 1) / lights[i].angle + 1);
				
			float4 lightViewPosition = mul(float4(input.worldPosition, 1), lights[i].view);
			lightViewPosition = mul(lightViewPosition, lights[i].projection);
			
			lightViewPosition.xyz /= lightViewPosition.w;
			
			if( lightViewPosition.x < -1.0f || lightViewPosition.x > 1.0f ||
				lightViewPosition.y < -1.0f || lightViewPosition.y > 1.0f ||
				lightViewPosition.z < 0.0f  || lightViewPosition.z > 1.0f )
				break;
			
			float2 depthMapTexCoord = float2(lightViewPosition.x / 2.0f + 0.5f,
				-lightViewPosition.y / 2.0f + 0.5f);
			
			float depthValue = depthMaps[i].Sample(mainSampler, depthMapTexCoord).r;
			float lightDepthValue = lightViewPosition.z;
			lightDepthValue -= 0.001f;
			
			light += lightOutput * (lightDepthValue < depthValue);
			break;
		}
	}

	return input.color * float4(light, 0);
}