#pragma once

class Model
{
public:
	static const size_t ConstantBufferCount = D3D11_COMMONSHADER_CONSTANT_BUFFER_HW_SLOT_COUNT - 1;

	static Model CreateScreen()
	{
		float vertices[]{
			-1, 1, 0,
			3, 1, 0,
			-1, -3, 0
		};

		float textureCoords[]{
			0, 0,
			2, 0,
			0, 2
		};

		unsigned int indices[]{
			0, 1, 2
		};

		Model screen;
		screen.SetVertices(vertices, 3);
		screen.SetTexcoords(textureCoords, 3);
		screen.SetIndices(indices, 3);

		return screen;
	}
	static Model CreateCube(float size = 1.0f)
	{
		float vertexData[] = {
			-size, size, -size,
			-size, -size, -size,

			-size, size, size,
			-size, -size, size,

			size, size, size,
			size, -size, size,

			size, size, -size,
			size, -size, -size,

			-size, size, -size,
			-size, -size, -size,

			-size, size, -size,
			size, size, -size,

			size, -size, -size,
			-size, -size, -size,
		};

		float normalData[] = {
			0, 1, 0,
			0, 1, 0,

			0, 1, 0,
			0, 1, 0,

			0, 1, 0,
			0, 1, 0,

			0, 1, 0,
			0, 1, 0,

			0, 1, 0,
			0, 1, 0,

			0, 1, 0,
			0, 1, 0,

			0, 1, 0,
			0, 1, 0,
		};

		float textureCoordData[] = {
			0, 0.3333f,
			0, 0.6666f,

			0.25f, 0.3334f,
			0.25f, 0.6666f,

			0.5f, 0.3334f,
			0.5f, 0.6666f,

			0.75f, 0.3334f,
			0.75f, 0.6666f,

			1, 0.3334f,
			1, 0.6666f,

			0.25f, 0,
			0.5f, 0,

			0.5f, 1,
			0.25f, 1,
		};

		unsigned int indexData[] = {
			// Left
			0, 2, 1,
			2, 3, 1,
			
			// Front
			2, 4, 3,
			3, 4, 5,

			// Right
			4, 6, 5,
			6, 7, 5,

			// Back
			6, 8, 7,
			7, 8, 9,

			// Up
			2, 10, 4,
			4, 10, 11,

			// Bottom
			3, 5, 12,
			3, 12, 13,
		};

		Model cube;
		cube.SetVertices(vertexData, 14);
		cube.SetNormals(normalData, 14);
		cube.SetTexcoords(textureCoordData, 14);
		cube.SetIndices(indexData, 36);

		return cube;
	}
	
	Model()
	{
		ZeroMemory(constantBuffers, sizeof(ID3D11Buffer*) * ConstantBufferCount);

		SetInstanceCount(1);
		SetPosition(0, 0, 0);
		SetRotation(0, 0, 0);
		SetScale(1, 1, 1);

		InitConstantBuffer(ConstantBufferCount - 1, engineBufferData, sizeof(XMMATRIX) * 3 + sizeof(XMVECTOR) + sizeof(LightData) * Light::MaxRenderableLightCount);
	}
	Model(const char* filename, bool generateNormals = false) : Model()
	{
		LoadMesh(filename, generateNormals);
	}
	~Model()
	{
		Clear();
	}

	void SetPosition(const float x, const float y, const float z, const size_t id = 0)
	{
		position[id] = XMFLOAT3(x, y, z);
	}
	void SetRotation(const float x, const float y, const float z, const size_t id = 0)
	{
		rotation[id] = XMFLOAT3(x, y, z);
	}
	void SetScale(const float x, const float y, const float z, const size_t id = 0)
	{
		scale[id] = XMFLOAT3(x, y, z);
	}

	void InitConstantBuffer(const size_t index, const void* data, const size_t size)
	{
		D3D11_BUFFER_DESC bufferDesc;
		ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));
		bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc.ByteWidth = (size / 16 + (size % 16 != 0)) * 16;
		bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		Engine::device->CreateBuffer(&bufferDesc, 0, &constantBuffers[index]);

		if (data != nullptr)
		{
			UpdateConstantBuffer(index, data, size);
		}
	}
	void UpdateConstantBuffer(const size_t index, const void* data, const size_t size, const size_t offset = 0)
	{
		D3D11_MAPPED_SUBRESOURCE subresource;
		ZeroMemory(&subresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

		Engine::deviceContext->Map(constantBuffers[index], 0, D3D11_MAP_WRITE_DISCARD, 0, &subresource);
		memcpy((char*)subresource.pData + offset, data, size);
		Engine::deviceContext->Unmap(constantBuffers[index], 0);
	}

	bool LoadMesh(const char* filename, bool generateNormals = false)
	{
		void* vertexData, *normalData, *texcoordData, *colorData, *indexData;
		size_t vertexCount, indexCount;

		if (!Model::LoadMesh(filename, vertexData, normalData, texcoordData, colorData, vertexCount, indexData, indexCount, generateNormals))
		{
			Engine::DebugError("Load", "Model", filename, "Couldn't load model due to failed mesh load");
			return false;
		}

		SetVertices(vertexData, vertexCount);
		SetNormals(normalData, vertexCount);
		SetTexcoords(texcoordData, vertexCount);
		SetColors(colorData, vertexCount);
		SetIndices(indexData, indexCount);

		Engine::ReleaseMemory(&vertexData);
		Engine::ReleaseMemory(&normalData);
		Engine::ReleaseMemory(&texcoordData);
		Engine::ReleaseMemory(&colorData);
		Engine::ReleaseMemory(&indexData);
		return true;
	}

	void SetGeneratedNormals(float* vertices, size_t vertexCount, unsigned int* indices, size_t indexCount)
	{
		float* normals = GenerateNormals(vertices, vertexCount, indices, indexCount);
		SetNormals(normals, vertexCount);
		Engine::ReleaseMemory(&normals);
	}
	void SetVertices(const void* data, const int vertexCount)
	{
		Engine::ReleaseObject(&vertexBuffer);

		D3D11_BUFFER_DESC vertexBufferDesc;
		ZeroMemory(&vertexBufferDesc, sizeof(D3D11_BUFFER_DESC));
		vertexBufferDesc.ByteWidth = sizeof(float) * 3 * vertexCount;
		vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

		Engine::device->CreateBuffer(&vertexBufferDesc, 0, &vertexBuffer);
		Engine::deviceContext->UpdateSubresource(vertexBuffer, 0, 0, data, 0, 0);
	}
	void SetIndices(const void* data, const int indexCount)
	{
		Engine::ReleaseObject(&indexBuffer);

		D3D11_BUFFER_DESC indexBufferDesc;
		ZeroMemory(&indexBufferDesc, sizeof(D3D11_BUFFER_DESC));
		indexBufferDesc.ByteWidth = sizeof(int) * indexCount;
		indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

		Engine::device->CreateBuffer(&indexBufferDesc, 0, &indexBuffer);
		Engine::deviceContext->UpdateSubresource(indexBuffer, 0, 0, data, 0, 0);
		this->indexCount = indexCount;
	}
	void SetTexcoords(const void* data, const int texcoordCount)
	{
		Engine::ReleaseObject(&texcoordBuffer);

		D3D11_BUFFER_DESC texcoordBufferDesc;
		ZeroMemory(&texcoordBufferDesc, sizeof(D3D11_BUFFER_DESC));
		texcoordBufferDesc.ByteWidth = sizeof(float) * 2 * texcoordCount;
		texcoordBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

		Engine::device->CreateBuffer(&texcoordBufferDesc, 0, &texcoordBuffer);
		Engine::deviceContext->UpdateSubresource(texcoordBuffer, 0, 0, data, 0, 0);
	}
	void SetNormals(const void* data, const int normalCount)
	{
		Engine::ReleaseObject(&normalBuffer);

		D3D11_BUFFER_DESC normalBufferDesc;
		ZeroMemory(&normalBufferDesc, sizeof(D3D11_BUFFER_DESC));
		normalBufferDesc.ByteWidth = sizeof(float) * 3 * normalCount;
		normalBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

		Engine::device->CreateBuffer(&normalBufferDesc, 0, &normalBuffer);
		Engine::deviceContext->UpdateSubresource(normalBuffer, 0, 0, data, 0, 0);
	}
	void SetColors(const void* data, const int colorCount)
	{
		Engine::ReleaseObject(&colorBuffer);

		D3D11_BUFFER_DESC colorBufferDesc;
		ZeroMemory(&colorBufferDesc, sizeof(D3D11_BUFFER_DESC));
		colorBufferDesc.ByteWidth = sizeof(float) * 3 * colorCount;
		colorBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

		Engine::device->CreateBuffer(&colorBufferDesc, 0, &colorBuffer);
		Engine::deviceContext->UpdateSubresource(colorBuffer, 0, 0, data, 0, 0);
	}
	void SetMaterial(Material* material)
	{
		this->defaultMaterial = material;
	}
	void SetInstanceCount(size_t instances)
	{
		Engine::ReleaseObject(&constantBuffers[ConstantBufferCount - 2]);

		position.resize(instances);
		rotation.resize(instances);
		scale.resize(instances);
		transforms.resize(instances);

		for (size_t i = 0; i < instances; i++)
		{
			SetScale(1, 1, 1, i);
		}

		InitConstantBuffer(ConstantBufferCount - 2, nullptr, sizeof(XMMATRIX) * instances);
	}

	void AssignConstants()
	{
		Engine::deviceContext->VSSetConstantBuffers(0, ConstantBufferCount, constantBuffers);
		Engine::deviceContext->PSSetConstantBuffers(0, ConstantBufferCount, constantBuffers);
		Engine::deviceContext->GSSetConstantBuffers(0, ConstantBufferCount, constantBuffers);
		Engine::deviceContext->HSSetConstantBuffers(0, ConstantBufferCount, constantBuffers);
		Engine::deviceContext->DSSetConstantBuffers(0, ConstantBufferCount, constantBuffers);
	}
	void UnassignResources()
	{
		void* cleanMemory[ConstantBufferCount];
		ZeroMemory(cleanMemory, sizeof(void*) * ConstantBufferCount);

		Engine::deviceContext->VSSetConstantBuffers(0, ConstantBufferCount, (ID3D11Buffer**)cleanMemory);
		Engine::deviceContext->PSSetConstantBuffers(0, ConstantBufferCount, (ID3D11Buffer**)cleanMemory);
		Engine::deviceContext->GSSetConstantBuffers(0, ConstantBufferCount, (ID3D11Buffer**)cleanMemory);
		Engine::deviceContext->HSSetConstantBuffers(0, ConstantBufferCount, (ID3D11Buffer**)cleanMemory);
		Engine::deviceContext->DSSetConstantBuffers(0, ConstantBufferCount, (ID3D11Buffer**)cleanMemory);
	}
	void Render(const bool applyTransform, const bool updateLights, Material* material = nullptr)
	{
		unsigned int offset = 0;

		if (applyTransform)
		{
			UpdateTransformMatrices();
		}
		AssignConstants();

		if (updateLights)
		{
			UpdateLights();
		}

		if (material)
		{
			material->AssignToDevice();
		}
		else
		{
			defaultMaterial->AssignToDevice();
		}

		Engine::deviceContext->IASetVertexBuffers(0, 1, &vertexBuffer, &VertexStride, &offset);
		Engine::deviceContext->IASetVertexBuffers(1, 1, &texcoordBuffer, &TexcoordStride, &offset);
		Engine::deviceContext->IASetVertexBuffers(2, 1, &normalBuffer, &VertexStride, &offset);
		Engine::deviceContext->IASetVertexBuffers(3, 1, &colorBuffer, &VertexStride, &offset);
		Engine::deviceContext->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);
		Engine::deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		Engine::deviceContext->DrawIndexedInstanced(indexCount, transforms.size(), 0, 0, 0);
		
		if (material)
		{
			material->UnassignFromDevice();
		}
		else
		{
			defaultMaterial->UnassignFromDevice();
		}
		UnassignResources();
	}
	void Render(Material* material)
	{
		Render(true, true, material);
	}
	virtual void Render()
	{
		Render(true, true);
	}
	void Clear()
	{
		Engine::ReleaseObject(&vertexBuffer);
		Engine::ReleaseObject(&indexBuffer);
		Engine::ReleaseObject(&texcoordBuffer);
		Engine::ReleaseObject(&normalBuffer);

		for (size_t i = 0; i < ConstantBufferCount; i++)
		{
			Engine::ReleaseObject(&constantBuffers[i]);
		}
	}

protected:
	static const unsigned int VertexStride = sizeof(float) * 3;
	static const unsigned int TexcoordStride = sizeof(float) * 2;

	static bool LoadMesh(const char* filename, void*& vertexData, void*& normalData, void*& texcoordData, void*& colorData, size_t& vertexCount, void*& indexData, size_t& indexCount, bool generateNormals = false);
	static float* GenerateNormals(float* vertices, size_t vertexCount, unsigned int* indices, size_t indexCount)
	{
		float* normals = new float[vertexCount * 3];
		ZeroMemory(normals, sizeof(float) * vertexCount * 3);

		size_t* normalBlend = new size_t[vertexCount];
		ZeroMemory(normalBlend, sizeof(size_t) * vertexCount);

		for (size_t i = 0; i < indexCount; i += 3)
		{
			size_t i1 = indices[i];
			size_t i2 = indices[i + 1];
			size_t i3 = indices[i + 2];

			float v1[3];
			memcpy(v1, vertices + i1 * 3, sizeof(float) * 3);

			float v2[3];
			memcpy(v2, vertices + i2 * 3, sizeof(float) * 3);

			float v3[3];
			memcpy(v3, vertices + i3 * 3, sizeof(float) * 3);

			float* v21 = Subtract3(v2, v1);
			float* v31 = Subtract3(v3, v1);
			float* n = CrossProduct3(v21, v31);

			if (normalBlend[i1] > 1)
			{
				normals[i1 * 3] = normals[i1 * 3] * ((float)normalBlend[i1] - 1 / normalBlend[i1]) + n[0] * (1.0f / normalBlend[i1]);
				normals[i1 * 3 + 1] = normals[i1 * 3 + 1] * ((float)normalBlend[i1] - 1 / normalBlend[i1]) + n[0] * (1.0f / normalBlend[i1]);
				normals[i1 * 3 + 2] = normals[i1 * 3 + 2] * ((float)normalBlend[i1] - 1 / normalBlend[i1]) + n[0] * (1.0f / normalBlend[i1]);
			}
			else
			{
				memcpy(normals + i1 * 3, n, sizeof(float) * 3);
			}

			if (normalBlend[i2] > 1)
			{
				normals[i2 * 3] = normals[i2 * 3] * ((float)normalBlend[i2] - 1 / normalBlend[i2]) + n[0] * (1.0f / normalBlend[i2]);
				normals[i2 * 3 + 1] = normals[i2 * 3 + 1] * ((float)normalBlend[i2] - 1 / normalBlend[i2]) + n[0] * (1.0f / normalBlend[i2]);
				normals[i2 * 3 + 2] = normals[i2 * 3 + 2] * ((float)normalBlend[i2] - 1 / normalBlend[i2]) + n[0] * (1.0f / normalBlend[i2]);
			}
			else
			{
				memcpy(normals + i2 * 3, n, sizeof(float) * 3);
			}

			if (normalBlend[i3] > 1)
			{
				normals[i3 * 3] = normals[i3 * 3] * ((float)normalBlend[i3] - 1 / normalBlend[i3]) + n[0] * (1.0f / normalBlend[i3]);
				normals[i3 * 3 + 1] = normals[i3 * 3 + 1] * ((float)normalBlend[i3] - 1 / normalBlend[i3]) + n[0] * (1.0f / normalBlend[i3]);
				normals[i3 * 3 + 2] = normals[i3 * 3 + 2] * ((float)normalBlend[i3] - 1 / normalBlend[i3]) + n[0] * (1.0f / normalBlend[i3]);
			}
			else
			{
				memcpy(normals + i3 * 3, n, sizeof(float) * 3);
			}

			normalBlend[i1]++;
			normalBlend[i2]++;
			normalBlend[i3]++;

			Engine::ReleaseMemory(&v21);
			Engine::ReleaseMemory(&v31);
			Engine::ReleaseMemory(&n);
		}

		Engine::ReleaseMemory(&normalBlend);
		return normals;
	}
	static float* CrossProduct3(float* vector0, float* vector1)
	{
		float* result = new float[3];

		result[0] = vector0[1] * vector1[2] - vector0[2] * vector1[1];
		result[1] = vector0[0] * vector1[2] - vector0[2] * vector1[0];
		result[2] = vector0[0] * vector1[1] - vector0[1] * vector1[0];

		return result;
	}
	static float* Subtract3(float* vector0, float* vector1)
	{
		float* result = new float[3];

		result[0] = vector1[0] - vector0[0];
		result[1] = vector1[1] - vector0[1];
		result[2] = vector1[2] - vector0[2];

		return result;
	}

	size_t indexCount;
	std::vector<XMFLOAT3> position;
	std::vector<XMFLOAT3> rotation;
	std::vector<XMFLOAT3> scale;
	std::vector<XMMATRIX> transforms;
	XMMATRIX engineBufferData[2];

	ID3D11Buffer* vertexBuffer = nullptr;
	ID3D11Buffer* indexBuffer = nullptr;
	ID3D11Buffer* texcoordBuffer = nullptr;
	ID3D11Buffer* normalBuffer = nullptr;
	ID3D11Buffer* colorBuffer = nullptr;

	Material* defaultMaterial;
	ID3D11Buffer* constantBuffers[ConstantBufferCount];

	void UpdateTransformMatrices()
	{
		for (size_t i = 0; i < transforms.size(); i++)
		{
			transforms[i] = XMMatrixTranspose(XMMatrixScaling(scale[i].x, scale[i].y, scale[i].z)
				* XMMatrixRotationRollPitchYaw(rotation[i].x * Engine::DegToRad, rotation[i].y * Engine::DegToRad, rotation[i].z * Engine::DegToRad)
				* XMMatrixTranslation(position[i].x, position[i].y, position[i].z));
		}
		UpdateConstantBuffer(ConstantBufferCount - 2, transforms.data(), sizeof(XMMATRIX) * transforms.size());

		engineBufferData[0] = XMMatrixTranspose(Engine::GetCamera()->GetViewMatrix());
		engineBufferData[1] = XMMatrixTranspose(Engine::GetCamera()->GetProjectionMatrix());

		UpdateConstantBuffer(ConstantBufferCount - 1, engineBufferData, sizeof(XMMATRIX) * 2);
	}
	void UpdateLights()
	{
		D3D11_MAPPED_SUBRESOURCE subresource;
		ZeroMemory(&subresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

		Engine::deviceContext->Map(constantBuffers[ConstantBufferCount - 1], 0, D3D11_MAP_WRITE_NO_OVERWRITE, 0, &subresource);
		Light::GetRenderableLightCount((char*)subresource.pData + sizeof(XMMATRIX) * 2);
		Light::GetLightData((char*)subresource.pData + sizeof(XMMATRIX) * 2 + sizeof(XMVECTOR));
		Engine::deviceContext->Unmap(constantBuffers[ConstantBufferCount - 1], 0);
	}
};
