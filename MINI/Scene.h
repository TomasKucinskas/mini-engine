#pragma once

class Scene
{
	friend class Engine;

public:
	void Add(Model* model)
	{
		objects.push_back(model);
	}

	void Clear()
	{
		objects.clear();
	}

protected:
	std::vector<Model*> objects;

	void Render()
	{
		for (size_t i = 0; i < objects.size(); i++)
		{
			objects[i]->Render();
		}
	}
};