#pragma once

#include <chrono>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

#define _XM_NO_INTRINSICS_

#include <d3d11.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>
#include <Unknwn.h>

#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "d3dcompiler.lib")
#pragma comment (lib, "dxgi.lib")
#pragma comment (lib, "dxguid.lib")

using namespace DirectX;

class Camera;
class Scene;
class Texture;
class Engine
{
public:
	static const float PI;
	static const float DegToRad;
	static const float RadToDeg;

	static const XMFLOAT3 DefaultPosition;
	static const XMFLOAT3 DefaultRotation;
	static const XMFLOAT3 DefaultScale;
	static const XMFLOAT3 DefaultDirection;
	static const XMFLOAT3 DefaultForward;
	static const XMFLOAT3 DefaultUp;
	static const XMFLOAT3 DefaultRight;
	static const float DefaultFOV;
	static const float DefaultAspectRatio;
	static const float DefaultNearClip;
	static const float DefaultFarClip;

	static const DXGI_FORMAT FormatRGBA;
	static const DXGI_FORMAT FormatDepth;
	static const DXGI_FORMAT FormatDepthView;
	static const DXGI_FORMAT FormatDepthResource;

	static HWND hwnd;
	static ID3D11Device* device;
	static ID3D11DeviceContext* deviceContext;

	static void Initialize(const char* title, int width, int height, bool fullscreen, bool vsync = false);
	static void Deinitialize();

	static size_t GetHeight();
	static size_t GetWidth();

	static Camera* GetCamera();
	static Scene* GetScene();
	static Texture* GetRenderTarget();
	static float GetTime();
	static float GetDeltaTime();

	static void SetCamera(Camera* camera);
	static void SetScene(Scene* scene);
	static void SetRenderTarget(Texture* renderTexture = nullptr);

	static void UpdateTime();

	static bool Run();
	static void Quit();

	static void Clear();
	static void GeometryPass();
	static void Present();
	static void Render();

	template<class T> static void ReleaseObject(T** o);
	template<class T> static void ReleaseMemory(T** o);

	static std::wstring ToWString(const char* text);

	template<class T> static T Clamp(T& value, T min, T max);

	template<class T> static void Debug(T& data, bool endline = true);
	template<class T> static void DebugArray(T* data, size_t count, const char* label = nullptr, size_t group = 0);

	static void DebugMessage(const char* action, const char* subject, const char* context, const char* messageType, const char* message, const char* details);
	static void DebugLog(const char* action, const char* subject, const char* context, const char* message, const char* details = nullptr);
	static void DebugError(const char* action, const char* subject, const char* context, const char* message, const char* details = nullptr);

private:
	static DXGI_SWAP_CHAIN_DESC swapChainDesc;
	static IDXGISwapChain* swapChain;
	static ID3D11Texture2D* backBuffer;
	static ID3D11RenderTargetView* renderTarget;
	static ID3D11Texture2D* depthStencilBuffer;
	static ID3D11DepthStencilView* depthStencilView;
	static D3D11_VIEWPORT viewport;

	static Camera* currentCamera;
	static Texture* currentRenderTexture;
	static Scene* currentScene;
	static float clearColor[4];
	static bool vsync;

	static float time;
	static float deltaTime;
	static void* lastTimePoint;

	static MSG message;

	static LRESULT CALLBACK wndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
};

template<class T>
void Engine::ReleaseObject(T** o)
{
	if (*o != nullptr)
	{
		IUnknown* p = *reinterpret_cast<IUnknown * *>(o);
		p->Release();
		*o = nullptr;
	}
	else
	{
		DebugLog("Release", "Object", nullptr, "Attempt to release a null object");
	}
}

template<class T>
void Engine::ReleaseMemory(T** o)
{
	if (*o != nullptr)
	{
		delete[] * o;
		*o = nullptr;
	}
	else
	{
		DebugLog("Release", "Object", nullptr, "Attempt to release uninitialized memory");
	}
}

template<class T>
T Engine::Clamp(T& value, T min, T max)
{
	if (value > max) return max;
	if (value < min) return min;
	return value;
}

template<class T>
void Engine::Debug(T& data, bool endline)
{
#ifndef NDEBUG
	std::stringstream ss;
	ss << data;
	OutputDebugString(ss.str().c_str());
	if (endline) OutputDebugString("\r\n");
#endif
}

template<class T>
void Engine::DebugArray(T* data, size_t count, const char* label, size_t group)
{
	Debug("[Output Array] ", false);

	if (label != nullptr)
	{
		Debug(label, false);
		Debug(":", false);
	}

	Debug("");

	for (size_t i = 0; i < count; i++)
	{
		Debug(data[i], false);

		if (group > 0)
		{
			if (!(group - 1 - (i % group)))
			{
				Debug("");
			}
			else
			{
				Debug(" ", false);
			}
		}
		else
		{
			Debug("");
		}
	}

	Debug("");
}

#include "Texture.h"
#include "Material.h"
#include "Camera.h"
#include "Light.h"
#include "Model.h"
#include "Scene.h"
