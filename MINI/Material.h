#pragma once

class Material
{
public:
	static const size_t TextureCount = D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT;

	static const std::string DepthBufferShader;
	static const std::string Unlit2DShader;
	static const std::string Unlit3DShader;

	Material()
	{
		ZeroMemory(inputElementDesc, sizeof(D3D11_INPUT_ELEMENT_DESC) * 4);
		inputElementDesc[0].SemanticName = "POSITION";
		inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
		inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;

		inputElementDesc[1].SemanticName = "TEXCOORD";
		inputElementDesc[1].Format = DXGI_FORMAT_R32G32_FLOAT;
		inputElementDesc[1].InputSlot = 1;
		inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;

		inputElementDesc[2].SemanticName = "NORMAL";
		inputElementDesc[2].Format = DXGI_FORMAT_R32G32B32_FLOAT;
		inputElementDesc[2].InputSlot = 2;
		inputElementDesc[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;

		inputElementDesc[3].SemanticName = "COLOR";
		inputElementDesc[3].Format = DXGI_FORMAT_R32G32B32_FLOAT;
		inputElementDesc[3].InputSlot = 3;
		inputElementDesc[3].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;

		ZeroMemory(&rasterDesc, sizeof(D3D11_RASTERIZER_DESC));
		rasterDesc.CullMode = D3D11_CULL_BACK;
		rasterDesc.DepthClipEnable = true;
		rasterDesc.FillMode = D3D11_FILL_SOLID;

		if (FAILED(Engine::device->CreateRasterizerState(&rasterDesc, &rasterState)))
		{
			Engine::DebugError("Create", "Material", nullptr, "Could not create rasterizer state");
		}

		ZeroMemory(&samplerDesc, sizeof(D3D11_SAMPLER_DESC));
		samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.MipLODBias = 0.0f;
		samplerDesc.MaxAnisotropy = 1;
		samplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		samplerDesc.MinLOD = -FLT_MAX;
		samplerDesc.MaxLOD = FLT_MAX;

		if (FAILED(Engine::device->CreateSamplerState(&samplerDesc, &textureSampler)))
		{
			Engine::DebugError("Create", "Material", nullptr, "Could not create sampler state");
		}

		ZeroMemory(&depthStencilDesc, sizeof(D3D11_DEPTH_STENCIL_DESC));
		depthStencilDesc.DepthEnable = true;
		depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
		depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
		depthStencilDesc.StencilEnable = true;
		depthStencilDesc.StencilReadMask = 0xff;
		depthStencilDesc.StencilWriteMask = 0xff;
		depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
		depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
		depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
		depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

		if (FAILED(Engine::device->CreateDepthStencilState(&depthStencilDesc, &depthStencilState)))
		{
			Engine::DebugError("Create", "Material", nullptr, "Could not create depth stencil state");
		}
	}

	Material(const char* filename) : Material()
	{
		ZeroMemory(textures, sizeof(ID3D11ShaderResourceView*) * TextureCount);
		Load(filename);
	}

	Material(std::string shader, const char* context = nullptr) : Material()
	{
		ZeroMemory(textures, sizeof(ID3D11ShaderResourceView*) * TextureCount);
		Load(shader, context);
	}

	~Material()
	{
		Engine::ReleaseObject(&vertexShader);
		Engine::ReleaseObject(&pixelShader);
		Engine::ReleaseObject(&geometryShader);
		Engine::ReleaseObject(&hullShader);
		Engine::ReleaseObject(&domainShader);

		Engine::ReleaseObject(&inputLayout);
		Engine::ReleaseObject(&rasterState);
		Engine::ReleaseObject(&textureSampler);
		Engine::ReleaseObject(&depthStencilState);
	}

	void Load(const char* filename)
	{
		if (filename == nullptr)
		{
			Engine::DebugError("Load", "Shader", filename, "Filename is null");
		}

		std::ifstream file(filename);
		std::stringstream buffer;
		buffer << file.rdbuf();
		Load(buffer.str(), filename);
	}

	void Load(std::string shader, const char* context = nullptr)
	{
		Engine::ReleaseObject(&vertexShader);
		Engine::ReleaseObject(&pixelShader);
		Engine::ReleaseObject(&geometryShader);
		Engine::ReleaseObject(&hullShader);
		Engine::ReleaseObject(&domainShader);
		Engine::ReleaseObject(&inputLayout);

		ID3DBlob** shaderBlobs = new ID3DBlob*[5];
		ZeroMemory(shaderBlobs, sizeof(ID3DBlob*) * 5);

		if (CompileShader(context, shader, "mainVS", "vs_5_0", shaderBlobs[0]))
		{
			if (FAILED(Engine::device->CreateVertexShader(shaderBlobs[0]->GetBufferPointer(), shaderBlobs[0]->GetBufferSize(), 0, &vertexShader)))
			{
				Engine::DebugError("Load", "Shader", context, "Could not create vertex shader");
			}

			if (FAILED(Engine::device->CreateInputLayout(inputElementDesc, 4, shaderBlobs[0]->GetBufferPointer(), shaderBlobs[0]->GetBufferSize(), &inputLayout)))
			{
				Engine::DebugError("Load", "Shader", context, "Could not create input layout");
			}
		}

		if (CompileShader(context, shader, "mainPS", "ps_5_0", shaderBlobs[1]))
		{
			if (FAILED(Engine::device->CreatePixelShader(shaderBlobs[1]->GetBufferPointer(), shaderBlobs[1]->GetBufferSize(), 0, &pixelShader)))
			{
				Engine::DebugError("Load", "Shader", context, "Could not create pixel shader");
			}
		}

		if (CompileShader(context, shader, "mainGS", "gs_5_0", shaderBlobs[2]))
		{
			Engine::device->CreateGeometryShader(shaderBlobs[2]->GetBufferPointer(), shaderBlobs[2]->GetBufferSize(), 0, &geometryShader);
		}

		if (CompileShader(context, shader, "mainHS", "hs_5_0", shaderBlobs[3]))
		{
			Engine::device->CreateHullShader(shaderBlobs[3]->GetBufferPointer(), shaderBlobs[3]->GetBufferSize(), 0, &hullShader);
		}

		if (CompileShader(context, shader, "mainDS", "ds_5_0", shaderBlobs[4]))
		{
			Engine::device->CreateDomainShader(shaderBlobs[4]->GetBufferPointer(), shaderBlobs[4]->GetBufferSize(), 0, &domainShader);
		}

		for (size_t i = 0; i < 5; i++)
		{
			Engine::ReleaseObject(&shaderBlobs[i]);
		}
		Engine::ReleaseMemory(&shaderBlobs);
	}

	void AssignToDevice()                                   
	{
		if (vertexShader != nullptr)
		{
			Engine::deviceContext->IASetInputLayout(inputLayout);
			Engine::deviceContext->VSSetShader(vertexShader, 0, 0);
		}

		if (pixelShader != nullptr)
		{
			Engine::deviceContext->PSSetShader(pixelShader, 0, 0);
			Engine::deviceContext->PSSetSamplers(0, 1, &textureSampler);
		}

		if (geometryShader != nullptr)
		{
			Engine::deviceContext->GSSetShader(geometryShader, 0, 0);
		}

		if (hullShader != nullptr)
		{
			Engine::deviceContext->HSSetShader(hullShader, 0, 0);
		}

		if (domainShader != nullptr)
		{
			Engine::deviceContext->DSSetShader(domainShader, 0, 0);
		}

		if (rasterState != nullptr)
		{
			Engine::deviceContext->RSSetState(rasterState);
		}

		if (depthStencilState != nullptr)
		{
			Engine::deviceContext->OMSetDepthStencilState(depthStencilState, 1);
		}

		Engine::deviceContext->VSSetShaderResources(0, TextureCount, textures);
		Engine::deviceContext->PSSetShaderResources(0, TextureCount, textures);
		Engine::deviceContext->GSSetShaderResources(0, TextureCount, textures);
		Engine::deviceContext->HSSetShaderResources(0, TextureCount, textures);
		Engine::deviceContext->DSSetShaderResources(0, TextureCount, textures);
	}

	bool GetZBuffer()
	{
		return depthStencilDesc.DepthEnable;
	}

	void SetZBuffer(bool enabled)
	{
		Engine::ReleaseObject(&depthStencilState);
		depthStencilDesc.DepthEnable = enabled;
		Engine::device->CreateDepthStencilState(&depthStencilDesc, &depthStencilState);
	}

	void SetTexture(const size_t index, Texture* texture)
	{
		if (texture)
		{
			textures[index] = (ID3D11ShaderResourceView*)texture->GetView();
		}
		else
		{
			textures[index] = nullptr;
		}
	}

	void UnassignFromDevice()
	{
		void* cleanMemory[TextureCount];
		ZeroMemory(cleanMemory, sizeof(void*) * TextureCount);

		Engine::deviceContext->VSSetShaderResources(0, TextureCount, (ID3D11ShaderResourceView**)cleanMemory);
		Engine::deviceContext->PSSetShaderResources(0, TextureCount, (ID3D11ShaderResourceView**)cleanMemory);
		Engine::deviceContext->GSSetShaderResources(0, TextureCount, (ID3D11ShaderResourceView**)cleanMemory);
		Engine::deviceContext->HSSetShaderResources(0, TextureCount, (ID3D11ShaderResourceView**)cleanMemory);
		Engine::deviceContext->DSSetShaderResources(0, TextureCount, (ID3D11ShaderResourceView**)cleanMemory);
	}

protected:
	D3D11_INPUT_ELEMENT_DESC inputElementDesc[4];
	D3D11_RASTERIZER_DESC rasterDesc;
	D3D11_SAMPLER_DESC samplerDesc;
	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;

	ID3D11VertexShader* vertexShader = nullptr;
	ID3D11PixelShader* pixelShader = nullptr;
	ID3D11GeometryShader* geometryShader = nullptr;
	ID3D11HullShader* hullShader = nullptr;
	ID3D11DomainShader* domainShader = nullptr;

	ID3D11InputLayout* inputLayout = nullptr;
	ID3D11RasterizerState* rasterState = nullptr;
	ID3D11SamplerState* textureSampler = nullptr;
	ID3D11DepthStencilState* depthStencilState = nullptr;

	ID3D11ShaderResourceView* textures[TextureCount];

	static bool CompileShader(const char* context, const std::string& shader, const char* entry, const char* target, ID3DBlob*& blob)
	{
		ID3DBlob* errors = nullptr;

		if (FAILED(D3DCompile(shader.c_str(), shader.size(), 0, 0, 0, entry, target, D3DCOMPILE_DEBUG, 0, &blob, &errors)))
		{
			Engine::DebugError("Compile", "Shader", context, nullptr, (const char*)errors->GetBufferPointer());
			return false;
		}

		return true;
	}
};
