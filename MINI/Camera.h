#pragma once

class Camera
{
public:
	Camera(const XMVECTOR position = XMLoadFloat3(&Engine::DefaultPosition), const XMVECTOR rotation = XMLoadFloat3(&Engine::DefaultRotation), const float fov = Engine::DefaultFOV,
		const float aspectRatio = Engine::DefaultAspectRatio, const float nearClip = Engine::DefaultNearClip, const float farClip = Engine::DefaultFarClip)
	{
		SetColor(XMLoadFloat3(&defaultColor));
		SetPosition(position);
		SetRotation(rotation);
		SetProjection(fov, aspectRatio, nearClip, farClip);
	}

	Camera(const float px, const float py, const float pz, const float rx = Engine::DefaultRotation.x, const float ry = Engine::DefaultRotation.y, const float rz = Engine::DefaultRotation.z,
		const float fov = Engine::DefaultFOV, const float aspectRatio = Engine::DefaultAspectRatio, const float nearClip = Engine::DefaultNearClip, const float farClip = Engine::DefaultFarClip)
	{
		XMFLOAT3 position(px, py, pz);
		XMFLOAT3 rotation(rx, ry, rz);

		SetColor(XMLoadFloat3(&defaultColor));
		SetPosition(XMLoadFloat3(&position));
		SetRotation(XMLoadFloat3(&rotation));
		SetProjection(fov, aspectRatio, nearClip, farClip);
	}

	XMVECTOR GetColor() const
	{
		return color;
	}
	XMVECTOR GetPosition() const
	{
		return position;
	}
	XMVECTOR GetRotation() const
	{
		return rotation;
	}
	XMVECTOR GetForward() const
	{
		return XMVector3Transform(XMLoadFloat3(&Engine::DefaultForward), XMMatrixRotationRollPitchYaw(rotation.vector4_f32[0], rotation.vector4_f32[1], rotation.vector4_f32[2]));
	}
	XMVECTOR GetUp() const
	{
		return XMVector3Transform(XMLoadFloat3(&Engine::DefaultUp), XMMatrixRotationRollPitchYaw(rotation.vector4_f32[0], rotation.vector4_f32[1], rotation.vector4_f32[2]));
	}
	XMVECTOR GetRight() const
	{
		return XMVector3Transform(XMLoadFloat3(&Engine::DefaultRight), XMMatrixRotationRollPitchYaw(rotation.vector4_f32[0], rotation.vector4_f32[1], rotation.vector4_f32[2]));
	}
	XMMATRIX GetViewMatrix() const
	{
		return view;
	}
	XMMATRIX GetProjectionMatrix() const
	{
		return projection;
	}
	float GetAspectRatio()
	{
		return aspectRatio;
	}
	float GetFieldOfView()
	{
		return fieldOfView;
	}
	float GetNearClip()
	{
		return nearClip;
	}
	float GetFarClip()
	{
		return farClip;
	}

	void SetColor(const float r, const float g, const float b)
	{
		XMFLOAT3 color(r, g, b);
		SetColor(XMLoadFloat3(&color));
	}
	void SetColor(const XMVECTOR color)
	{
		this->color = color;
	}
	void SetPosition(const float x, const float y, const float z)
	{
		XMFLOAT3 position(x, y, z);
		SetPosition(XMLoadFloat3(&position));
	}
	void SetPosition(const XMVECTOR position)
	{
		this->position = position;
		UpdateViewMatrix();
	}
	void SetRotation(const float x, const float y, const float z)
	{
		XMFLOAT3 rotation(x, y, z);
		SetRotation(XMLoadFloat3(&rotation));
	}
	void SetRotation(const XMVECTOR rotation)
	{
		this->rotation = rotation * Engine::DegToRad;
		UpdateViewMatrix();
	}
	void SetProjection(const float fieldOfView, const float aspectRatio, const float nearClip, const float farClip)
	{
		this->fieldOfView = fieldOfView;
		this->aspectRatio = aspectRatio;
		this->nearClip = nearClip;
		this->farClip = farClip;
		UpdateProjectionMatrix();
	}
	void SetAspectRatio(const float aspectRatio)
	{
		SetProjection(fieldOfView, aspectRatio, nearClip, farClip);
	}
	void SetFieldOfView(const float fieldOfView)
	{
		SetProjection(fieldOfView, aspectRatio, nearClip, farClip);
	}
	void SetNearClip(const float nearClip)
	{
		SetProjection(fieldOfView, aspectRatio, nearClip, farClip);
	}
	void SetFarClip(const float farClip)
	{
		SetProjection(fieldOfView, aspectRatio, nearClip, farClip);
	}

protected:
	static XMFLOAT3 defaultColor;

	XMVECTOR color;
	XMVECTOR position;
	XMVECTOR rotation;
	float fieldOfView;
	float aspectRatio;
	float nearClip;
	float farClip;

	XMMATRIX view;
	XMMATRIX projection;

	void UpdateViewMatrix()
	{
		view = XMMatrixLookToLH(position, GetForward(), GetUp());
	}
	void UpdateProjectionMatrix()
	{
		projection = XMMatrixPerspectiveFovLH(fieldOfView * Engine::DegToRad, aspectRatio, nearClip, farClip); // Tomas Gejus
	}
};
