#pragma once

class LightData
{
protected:
	XMMATRIX view;
	XMMATRIX projection;

	XMFLOAT3 position;
	float range;

	XMFLOAT3 color;
	float intensity;

	XMFLOAT3 direction;
	float angle;

	// Yep. It's unnecessarily complicated for now but implemented it this way just to see if it works
	union
	{
		struct
		{
			size_t type : 31;
			size_t enabled : 1;
		};
		char bytes[16];
	} Metadata;
};

class Light : LightData
{
public:
	enum TYPE
	{
		TYPE_DIRECTIONAL,
		TYPE_POINT,
		TYPE_SPOT
	};

	static const size_t MaxRenderableLightCount = 8;

	static Light CreateDirectionalLight(float dirX, float dirY, float dirZ, float r, float g, float b, float intensity = DefaultIntensity)
	{
		return Light(TYPE_DIRECTIONAL, Engine::DefaultPosition.x, Engine::DefaultPosition.y, Engine::DefaultPosition.z, r, g, b, dirX, dirY, dirZ, DefaultRange, intensity);
	}

	static Light CreatePointLight(float x, float y, float z, float range, float r, float g, float b, float intensity = DefaultIntensity)
	{
		return Light(TYPE_POINT, x, y, z, r, g, b, Engine::DefaultForward.x, Engine::DefaultForward.y, Engine::DefaultForward.z, range, intensity);
	}

	static Light CreateSpotLight(float x, float y, float z, float range, float dirX, float dirY, float dirZ, float r, float g, float b, float angle = DefaultAngle , float intensity = DefaultIntensity)
	{
		return Light(TYPE_SPOT, x, y, z, r, g, b, dirX, dirY, dirZ, range, intensity, angle);
	}

	static size_t GetRenderableLightCount()
	{
		return min(MaxRenderableLightCount, lights.size());
	}

	static void GetRenderableLightCount(void* data)
	{
		*((size_t*)data) = GetRenderableLightCount();
	}

	static void GetLightData(void* data)
	{
		size_t renderableLightCount = GetRenderableLightCount();

		ZeroMemory(data, sizeof(LightData) * renderableLightCount);

		for (size_t i = 0; i < renderableLightCount; i++)
		{
			memcpy(&((char*)data)[sizeof(LightData) * i], lights[i], sizeof(LightData));
		}
	}

	static void UpdateLights();

	Light() : lightView(0, 0, 0)
	{
		ZeroMemory(this, sizeof(Light));
	}

	Light(TYPE type, float x, float y, float z, float r, float g, float b, float rx, float ry, float rz,
		float range = DefaultRange, float intensity = DefaultIntensity, float angle = DefaultAngle) : shadowMap(1024, 1024, nullptr, Texture::TYPE_DEPTH, true)
	{
		lights.push_back(this);

		position = XMFLOAT3(x, y, z);
		color = XMFLOAT3(r, g, b);

		this->Metadata.type = type;
		this->range = range;
		this->intensity = intensity;
		this->angle = angle / 360.0f;

		lightView = Camera(x, y, z, rx, ry, rz, angle, 1, 0.1f, range);
		XMStoreFloat3(&this->direction, lightView.GetForward());
	}

	~Light()
	{
		for (size_t i = lights.size() - 1; i < lights.size(); i--)
		{
			if (lights[i] == this)
			{
				lights.erase(lights.begin() + i);
			}
		}
	}

	bool IsEnabled()
	{
		return !Metadata.enabled;
	}
	
	void SetPosition(XMVECTOR position)
	{
		this->position = XMFLOAT3(position.vector4_f32[0], position.vector4_f32[1], position.vector4_f32[2]);
		lightView.SetPosition(position);
	}

	void SetRotation(XMVECTOR direction)
	{
		lightView.SetRotation(direction.vector4_f32[0], direction.vector4_f32[1], direction.vector4_f32[2]);
		XMStoreFloat3(&this->direction, lightView.GetForward());
	}

	Texture* GetDepthMap;

protected:
	static const float DefaultRange;
	static const float DefaultIntensity;
	static const float DefaultAngle;

	static std::vector<Light*> lights;
	static LightData renderableLights[MaxRenderableLightCount];
	static ID3D11ShaderResourceView* shadowMaps[MaxRenderableLightCount];

	Texture shadowMap;
	Camera lightView;

	void RenderShadowMap();
};
