#pragma once

class Texture
{
	friend class Engine;

public:
	enum TYPE
	{
		TYPE_RGBA,
		TYPE_DEPTH
	};

	Texture()
	{
		ZeroMemory(this, sizeof(Texture));
	}

	Texture(size_t width, size_t height, void* data = nullptr, TYPE type = TYPE_RGBA, bool renderTarget = false) : Texture()
	{
		Load(width, height, data, type, renderTarget);
	}

	Texture(const char* filename) : Texture()
	{
		Load(filename);
	}

	~Texture()
	{
		Engine::ReleaseObject(&texture);
		Engine::ReleaseObject(&textureResourceView);
		Engine::ReleaseObject(&renderTargetView);
	}

	void Load(size_t width, size_t height, void* data = nullptr, TYPE type = TYPE_RGBA, bool renderTarget = false)
	{
		Engine::ReleaseObject(&texture);
		Engine::ReleaseObject(&textureResourceView);

		this->type = type;

		ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));
		textureDesc.Width = width;
		textureDesc.Height = height;
		textureDesc.MipLevels = 1;
		textureDesc.ArraySize = 1;
		textureDesc.SampleDesc.Count = 1;
		textureDesc.Usage = D3D11_USAGE_DEFAULT;
		textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;

		D3D11_SHADER_RESOURCE_VIEW_DESC textureViewDesc;
		ZeroMemory(&textureViewDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
		textureViewDesc.Texture2D.MipLevels = textureDesc.MipLevels;
		textureViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;

		ZeroMemory(&viewport, sizeof(D3D11_VIEWPORT));
		viewport.Width = width;
		viewport.Height = height;
		viewport.MaxDepth = 1;

		D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
		ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
		depthStencilViewDesc.Format = Engine::FormatDepthView;
		depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;

		switch (type)
		{
		case Texture::TYPE_RGBA:
			textureDesc.Format = Engine::FormatRGBA;
			textureViewDesc.Format = textureDesc.Format;
			break;
		case Texture::TYPE_DEPTH:
			textureDesc.Format = Engine::FormatDepth;
			textureDesc.BindFlags |= D3D11_BIND_DEPTH_STENCIL;
			textureViewDesc.Format = Engine::FormatDepthResource;
			break;
		default:
			Engine::DebugError("Load", "Texture", nullptr, "Cannot decide texture format from unknown texture type");
			break;
		}

		if (data != nullptr)
		{
			D3D11_SUBRESOURCE_DATA textureSubresource;
			textureSubresource.pSysMem = data;

			switch (type)
			{
			case Texture::TYPE_RGBA:
				textureSubresource.SysMemPitch = width * 4;
				break;
			case Texture::TYPE_DEPTH:
				textureSubresource.SysMemPitch = width;
				break;
			default:
				Engine::DebugError("Load", "Texture", nullptr, "Cannot calculate texture size from unknown texture type");
				break;
			}

			textureSubresource.SysMemSlicePitch = textureSubresource.SysMemPitch * height;

			Engine::device->CreateTexture2D(&textureDesc, &textureSubresource, &texture);
		}
		else
		{
			Engine::device->CreateTexture2D(&textureDesc, nullptr, &texture);
		}

		Engine::device->CreateShaderResourceView(texture, &textureViewDesc, &textureResourceView);

		if (renderTarget)
		{
			switch (type)
			{
			case Texture::TYPE_RGBA:
				textureDesc.BindFlags |= D3D11_BIND_RENDER_TARGET;

				D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
				ZeroMemory(&renderTargetViewDesc, sizeof(D3D11_RENDER_TARGET_VIEW_DESC));
				renderTargetViewDesc.Format = textureDesc.Format;
				renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;

				Engine::device->CreateRenderTargetView(texture, &renderTargetViewDesc, &renderTargetView);

				D3D11_TEXTURE2D_DESC depthBufferDesc;
				ZeroMemory(&depthBufferDesc, sizeof(D3D11_TEXTURE2D_DESC));
				depthBufferDesc.Width = viewport.Width;
				depthBufferDesc.Height = viewport.Height;
				depthBufferDesc.MipLevels = 1;
				depthBufferDesc.ArraySize = 1;
				depthBufferDesc.Format = Engine::FormatDepth;
				depthBufferDesc.SampleDesc.Count = 1;
				depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;

				Engine::device->CreateTexture2D(&depthBufferDesc, 0, &depthStencilBuffer);
				Engine::device->CreateDepthStencilView(depthStencilBuffer, &depthStencilViewDesc, &depthStencilView);
				break;

			case Texture::TYPE_DEPTH:
				if (FAILED(Engine::device->CreateDepthStencilView(texture, &depthStencilViewDesc, &depthStencilView)))
				{
					Engine::DebugError("Create", "Render Texture", nullptr, "Could not create depth render texture");
				}
				break;

			default:
				Engine::DebugError("Load", "Texture", nullptr, "Cannot decide texture format from unknown texture type");
				break;
			}
		}
	}

	void Load(const char* filename, TYPE type = TYPE_RGBA)
	{
		size_t width, height;
		unsigned char* data = nullptr;
		this->type = type;

		switch (type)
		{
		case Texture::TYPE_RGBA:
			Texture::LoadBitmap(filename, data, width, height);
			Load(width, height, data);
			break;
		case Texture::TYPE_DEPTH:
			Engine::DebugError("Load", "Texture", filename, "Depth texture loading from file is not supported yet");
			break;
		default:
			Engine::DebugError("Load", "Texture", filename, "Unknown texture format specified");
			break;
		}

		Engine::ReleaseMemory(&data);
	}

	unsigned char* GetData()
	{
		D3D11_TEXTURE2D_DESC textureCopyDesc;
		memcpy(&textureCopyDesc, &textureDesc, sizeof(D3D11_TEXTURE2D_DESC));
		textureCopyDesc.BindFlags = 0;
		textureCopyDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
		textureCopyDesc.Usage = D3D11_USAGE_STAGING;

		ID3D11Texture2D* textureCopy;
		Engine::device->CreateTexture2D(&textureCopyDesc, nullptr, &textureCopy);
		Engine::deviceContext->CopyResource(textureCopy, texture);

		D3D11_MAPPED_SUBRESOURCE subresource;
		Engine::deviceContext->Map(textureCopy, 0, D3D11_MAP_READ, 0, &subresource);

		unsigned char* data = nullptr;

		switch (type)
		{
		case Texture::TYPE_RGBA:
			data = new unsigned char[textureDesc.Width * textureDesc.Height * 4];
			memcpy(data, subresource.pData, textureDesc.Width * textureDesc.Height * 4);
			break;
		case Texture::TYPE_DEPTH:
			data = new unsigned char[textureDesc.Width * textureDesc.Height];
			memcpy(data, subresource.pData, textureDesc.Width * textureDesc.Height);
			break;
		default:
			Engine::DebugError("Get", "Texture Data", nullptr, "Unknown texture format specified");
			break;
		}

		Engine::deviceContext->Unmap(textureCopy, 0);
		Engine::ReleaseObject(&textureCopy);

		return data;
	}
	size_t GetHeight()
	{
		return textureDesc.Height;
	}
	size_t GetWidth()
	{
		return textureDesc.Width;
	}
	void* GetView()
	{
		return textureResourceView;
	}

	void SetViewportRect(float x, float y, float width, float height)
	{
		viewport.TopLeftX = x;
		viewport.TopLeftY = y;
		viewport.Width = width;
		viewport.Height = height;
	}
	void SetViewportRect(float width, float height)
	{
		SetViewportRect(0, 0, width, height);
	}

	void AssignToRenderTarget()
	{
		switch (type)
		{
		case Texture::TYPE_RGBA:
		case Texture::TYPE_DEPTH:
			Engine::deviceContext->OMSetRenderTargets(1, &renderTargetView, depthStencilView);
			Engine::deviceContext->RSSetViewports(1, &viewport);
			break;

		default:
			Engine::DebugError("Assign", "Render Texture", nullptr, "Unknown texture format");
			break;
		}
	}

protected:
	static void LoadBitmap(const char* filename, unsigned char*& data, size_t& width, size_t& height);

	TYPE type;
	D3D11_TEXTURE2D_DESC textureDesc;
	D3D11_VIEWPORT viewport;

	ID3D11Texture2D* texture = nullptr;
	ID3D11ShaderResourceView* textureResourceView = nullptr;

	ID3D11RenderTargetView* renderTargetView = nullptr;
	ID3D11Texture2D* depthStencilBuffer = nullptr;
	ID3D11DepthStencilView* depthStencilView = nullptr;
};