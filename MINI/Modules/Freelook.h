#pragma once

#include "../Engine.h"

class Freelook : public Camera
{
public:
	inline Freelook(float lookSensitivity, float movementSensitivity, float boostPower,
		XMVECTOR position = XMLoadFloat3(&Engine::DefaultPosition),
		XMVECTOR rotation = XMLoadFloat3(&Engine::DefaultRotation),
		float fov = Engine::DefaultFOV, float aspectRatio = Engine::DefaultAspectRatio,
		float nearClip = Engine::DefaultNearClip, float farClip = Engine::DefaultFarClip)
		: lookSensitivity(lookSensitivity), movementSensitivity(movementSensitivity),
		boostPower(boostPower), lookX(0), lookY(0)
	{
		Camera(position, rotation, fov, aspectRatio, nearClip, farClip);

		RECT clientRect;
		GetClientRect(Engine::hwnd, &clientRect);

		POINT clientCenter;
		clientCenter.x = clientRect.left + (clientRect.right - clientRect.left) / 2;
		clientCenter.y = clientRect.left + (clientRect.right - clientRect.left) / 2;
		SetCursorPos(clientCenter.x, clientCenter.y);
		ShowCursor(false);
	}

	inline ~Freelook()
	{
		ShowCursor(true);
	}

	inline void Update()
	{
		RECT clientRect;
		GetClientRect(Engine::hwnd, &clientRect);

		POINT clientCenter;
		clientCenter.x = clientRect.left + (clientRect.right - clientRect.left) / 2;
		clientCenter.y = clientRect.left + (clientRect.right - clientRect.left) / 2;

		POINT cursorPos;
		GetCursorPos(&cursorPos);

		POINT cursorDelta;
		cursorDelta.x = cursorPos.x - clientCenter.x;
		cursorDelta.y = cursorPos.y - clientCenter.y;

		lookX += cursorDelta.x * lookSensitivity;
		lookY += cursorDelta.y * lookSensitivity;
		lookY = Engine::Clamp(lookY, -90.0f, 90.0f);

		SetCursorPos(clientCenter.x, clientCenter.y);
		
		SetRotation(lookY, lookX, 0);
		
		bool moveLeft = HIBYTE(GetKeyState(0x41)) || HIBYTE(GetKeyState(0x25));		// A or Left
		bool moveRight = HIBYTE(GetKeyState(0x44)) || HIBYTE(GetKeyState(0x27));	// D or Right
		bool moveForward = HIBYTE(GetKeyState(0x53)) || HIBYTE(GetKeyState(0x28));	// W or Up
		bool moveBack = HIBYTE(GetKeyState(0x57)) || HIBYTE(GetKeyState(0x26));		// S or Down
		bool boost = HIBYTE(GetKeyState(VK_SHIFT));
		
		float horizontalSpeed = (moveRight - moveLeft) * movementSensitivity * Engine::GetDeltaTime();
		float verticalSpeed = (moveBack - moveForward) * movementSensitivity * Engine::GetDeltaTime();

		horizontalSpeed *= boost ? boostPower : 1;
		verticalSpeed *= boost ? boostPower : 1;
		
		XMVECTOR horizontalMovement = XMVectorScale(GetRight(), horizontalSpeed);
		XMVECTOR verticalMovement = XMVectorScale(GetForward(), verticalSpeed);

		SetPosition(XMVectorAdd(GetPosition(), horizontalMovement));
		SetPosition(XMVectorAdd(GetPosition(), verticalMovement));
	}

private:
	float lookSensitivity;
	float movementSensitivity;
	float boostPower;

	float lookX;
	float lookY;
};