#pragma once

#include "../Engine.h"

class Skybox : public Model
{
public:
	Skybox() : Model()
	{
		float vertexData[] = {
			-1, 1, -1,
			-1, -1, -1,

			-1, 1, 1,
			-1, -1, 1,

			1, 1, 1,
			1, -1, 1,

			1, 1, -1,
			1, -1, -1,

			-1, 1, -1,
			-1, -1, -1,

			-1, 1, -1,
			1, 1, -1,

			1, -1, -1,
			-1, -1, -1,
		};

		float textureCoordData[] = {
			0, 0.3333f,
			0, 0.6666f,

			0.25f, 0.3334f,
			0.25f, 0.6666f,

			0.5f, 0.3334f,
			0.5f, 0.6666f,

			0.75f, 0.3334f,
			0.75f, 0.6666f,

			1, 0.3334f,
			1, 0.6666f,

			0.25f, 0,
			0.5f, 0,

			0.5f, 1,
			0.25f, 1,
		};

		unsigned int indexData[] = {
			0, 2, 1,
			2, 3, 1,

			2, 4, 3,
			3, 4, 5,

			4, 6, 5,
			6, 7, 5,

			6, 8, 7,
			7, 8, 9,

			2, 10, 4,
			4, 10, 11,

			3, 5, 12,
			3, 12, 13,
		};
		
		SetVertices(vertexData, 14);
		SetTexcoords(textureCoordData, 14);
		SetIndices(indexData, 36);
	}
};