#pragma once

#include "../Engine.h"

class Terrain : public Model
{
public:
	Terrain(Material* material, Texture* heightmap, float size, float height)
	{
		SetMaterial(material);

		unsigned char* textureData = heightmap->GetData();
		size_t textureWidth = heightmap->GetWidth();
		size_t textureHeight = heightmap->GetHeight();

		float* vertices = new float[textureWidth * textureHeight * 3];
		float* normals = new float[textureWidth * textureHeight * 3];
		unsigned long* indices = new unsigned long[textureWidth * textureHeight * 6];
		float* texcoords = new float[textureWidth * textureHeight * 2];

		int index = 0;
		for (int y = 0; y < textureHeight; y++)
		{
			for (int x = 0; x < textureWidth; x++)
			{
				int pixelIndex = x + y * textureWidth;

				vertices[pixelIndex * 3] = size * x / textureWidth;
				vertices[pixelIndex * 3 + 1] = textureData[pixelIndex * 4] / 255.0f * height;
				vertices[pixelIndex * 3 + 2] = size * y / textureHeight;

				normals[pixelIndex * 3] = 0;
				normals[pixelIndex * 3 + 1] = 1;
				normals[pixelIndex * 3 + 2] = 0;

				texcoords[pixelIndex * 2] = (float)x / textureWidth;
				texcoords[pixelIndex * 2 + 1] = (float)y / textureHeight;

				if (y + 1 < textureHeight)
				{
					int pixelNextRow = x + (y + 1) * textureWidth;

					if (x % 2)
					{
						indices[index++] = pixelIndex - 1;
						indices[index++] = pixelNextRow;
						indices[index++] = pixelIndex;

						if (x + 1 < textureWidth)
						{
							indices[index++] = pixelIndex;
							indices[index++] = pixelNextRow;
							indices[index++] = pixelIndex + 1;
						}
					}
					else
					{
						indices[index++] = pixelIndex;
						indices[index++] = pixelNextRow;
						indices[index++] = pixelNextRow + 1;

						if (x > 0)
						{
							indices[index++] = pixelNextRow - 1;
							indices[index++] = pixelNextRow;
							indices[index++] = pixelIndex;
						}
					}
				}
			}
		}

		SetVertices(vertices, textureWidth * textureHeight);
		SetNormals(normals, textureWidth * textureHeight);
		SetIndices(indices, textureWidth * textureHeight * 6);
		SetTexcoords(texcoords, textureWidth * textureHeight);

		Engine::ReleaseMemory(&textureData);
		Engine::ReleaseMemory(&vertices);
		Engine::ReleaseMemory(&indices);
		Engine::ReleaseMemory(&texcoords);
		Engine::ReleaseMemory(&normals);
	}
};