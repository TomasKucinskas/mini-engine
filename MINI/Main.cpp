#include <ctime>
#include <cmath>

#include "Engine/Engine.h"
#include "Modules/Freelook.h"
#include "Modules/Terrain.h"
#include "Modules/Skybox.h"

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	unsigned screenWidth = 550;
	unsigned screenHeight = 550;
	bool fullscreen = false;

	Engine::Initialize("MINI Engine", screenWidth, screenHeight, fullscreen);

	Freelook freelook = Freelook(0.1f, 20, 5.0f);
	Engine::SetCamera(&freelook);
	
	Texture liudas = Texture("..\\..\\Resources\\Liudas256.jpg");
	Texture terrainHeightmap = Texture("..\\..\\Resources\\3.bmp");
	Texture skyboxTexture = Texture("..\\..\\Resources\\Skybox.jpg");

	Material standard = Material("..\\..\\Resources\\Standard2.hlsl");
	Material depthBuffer = Material("..\\..\\Resources\\DepthBuffer.hlsl");
	Material skyboxMaterial = Material("..\\..\\Resources\\Skybox.hlsl");
	Material textured2D = Material(Material::Unlit2DShader);
	skyboxMaterial.SetZBuffer(false);
	textured2D.SetZBuffer(false);

	Skybox skybox = Skybox("..\\..\\Resources\\Skybox.jpg");
	skybox.SetMaterial(&skyboxMaterial);
	skybox.SetTexture(0, &skyboxTexture);

	Model ground;
	ground.LoadMesh("..\\..\\Resources\\TestCube.obj");
	ground.SetScale(10, 1, 10);
	ground.SetMaterial(&standard);
	ground.SetTexture(0, &liudas);
	
	Model model;
	model.LoadMesh("..\\..\\Resources\\liudapalme.obj");
	model.SetMaterial(&standard);
	model.SetTexture(0, &liudas);
	
	Scene scene;
	scene.Add(&skybox);
	scene.Add(&ground);
	scene.Add(&model);
	Engine::SetScene(&scene);

	Light light = Light::CreateSpotLight(0, 10, 0, 20, 90 * Engine::DegToRad, 0, 0, 0.33, 1, 0, 45, 5);
	Light light2 = Light::CreatePointLight(3, 1, 0, 5, 0, 0.33, 1, 1);
	Light light3 = Light::CreateDirectionalLight(-0.5, -1, 0, 1, 0.66, 0.16, 1);

	while (Engine::Run())
	{
		freelook.Update();
		//model.SetPosition(sin(Engine::GetTime()), 0, cos(Engine::GetTime()));

		//light.SetPosition(Engine::GetCamera()->GetPosition());
		//light.SetRotation(Engine::GetCamera()->GetRotation());
		
		Engine::Render();
	}

	Engine::Deinitialize();

	return 0;
}