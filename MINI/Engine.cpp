#include "Engine.h"

#define _USE_MATH_DEFINES
#include <cmath>

#define STB_IMAGE_IMPLEMENTATION
#include "3rd party/stb_image.h"

#include "3rd party/OBJ_Loader.h"

const float Engine::PI = M_PI;
const float Engine::DegToRad = Engine::PI / 180;
const float Engine::RadToDeg = 180 / Engine::PI;

const DirectX::XMFLOAT3 Engine::DefaultPosition = XMFLOAT3(0, 0, 0);
const DirectX::XMFLOAT3 Engine::DefaultRotation = XMFLOAT3(0, 0, 0);
const DirectX::XMFLOAT3 Engine::DefaultScale = XMFLOAT3(1, 1, 1);
const DirectX::XMFLOAT3 Engine::DefaultForward = XMFLOAT3(0, 0, 1);
const DirectX::XMFLOAT3 Engine::DefaultUp = XMFLOAT3(0, 1, 0);
const DirectX::XMFLOAT3 Engine::DefaultRight = XMFLOAT3(1, 0, 0);

const float Engine::DefaultFOV = 60;
const float Engine::DefaultAspectRatio = 1;
const float Engine::DefaultNearClip = 0.1f;
const float Engine::DefaultFarClip = 1000;

const DXGI_FORMAT Engine::FormatRGBA = DXGI_FORMAT_R8G8B8A8_UNORM;
const DXGI_FORMAT Engine::FormatDepth = DXGI_FORMAT_R24G8_TYPELESS;
const DXGI_FORMAT Engine::FormatDepthView = DXGI_FORMAT_D24_UNORM_S8_UINT;
const DXGI_FORMAT Engine::FormatDepthResource = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;

HWND Engine::hwnd = nullptr;
ID3D11Device* Engine::device;
ID3D11DeviceContext* Engine::deviceContext;

void Engine::Initialize(const char* title, int width, int height, bool fullscreen, bool vsync)
{
	Engine::vsync = vsync;

	WNDCLASS wc = {};
	wc.hInstance = GetModuleHandle(NULL);
	wc.lpszClassName = title;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.lpfnWndProc = wndProc;
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
	RegisterClass(&wc);

	hwnd = CreateWindow(wc.lpszClassName, title, WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU, 0, 0, width, height, NULL, NULL, wc.hInstance, NULL);
	SetWindowPos(hwnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOSIZE);
	ShowWindow(hwnd, SW_NORMAL);
	UpdateWindow(hwnd);

	lastTimePoint = new std::chrono::high_resolution_clock::time_point;
	*((std::chrono::time_point<std::chrono::high_resolution_clock>*)lastTimePoint) =
		std::chrono::high_resolution_clock::now();
	
	ZeroMemory(&swapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	swapChainDesc.BufferCount = 1;
	swapChainDesc.BufferDesc.Width = width;
	swapChainDesc.BufferDesc.Height = height;
	swapChainDesc.BufferDesc.Format = Engine::FormatRGBA;
	swapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.OutputWindow = hwnd;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.Windowed = !fullscreen;
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_11_0;

	D3D11CreateDeviceAndSwapChain(0, D3D_DRIVER_TYPE_HARDWARE, 0,
		D3D11_CREATE_DEVICE_DEBUG, &featureLevel, 1, D3D11_SDK_VERSION,
		&swapChainDesc, &swapChain, &device, 0, &deviceContext);

	swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&backBuffer);

	device->CreateRenderTargetView(backBuffer, 0, &renderTarget);

	D3D11_TEXTURE2D_DESC depthBufferDesc;
	ZeroMemory(&depthBufferDesc, sizeof(D3D11_TEXTURE2D_DESC));
	depthBufferDesc.Width = width;
	depthBufferDesc.Height = height;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = Engine::FormatDepth;
	depthBufferDesc.SampleDesc.Count = 1;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;

	device->CreateTexture2D(&depthBufferDesc, 0, &depthStencilBuffer);

	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
	depthStencilViewDesc.Format = Engine::FormatDepthView;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;

	device->CreateDepthStencilView(depthStencilBuffer, &depthStencilViewDesc, &depthStencilView);
	deviceContext->OMSetRenderTargets(1, &renderTarget, depthStencilView);

	ZeroMemory(&viewport, sizeof(D3D11_VIEWPORT));
	viewport.Width = width;
	viewport.Height = height;
	viewport.MaxDepth = 1;

	Engine::SetRenderTarget();
}

void Engine::Deinitialize()
{
	swapChain->SetFullscreenState(false, 0);

	Engine::ReleaseObject(&depthStencilView);
	Engine::ReleaseObject(&depthStencilBuffer);
	Engine::ReleaseObject(&renderTarget);
	Engine::ReleaseObject(&deviceContext);
	Engine::ReleaseObject(&device);
	Engine::ReleaseObject(&swapChain);
}

void Texture::LoadBitmap(const char* filename, unsigned char*& data, size_t& width, size_t& height)
{
	int bitmapWidth, bitmapHeight, bytesPerPixel;
	data = stbi_load(filename, &bitmapWidth, &bitmapHeight, &bytesPerPixel, STBI_default);
	width = bitmapWidth;
	height = bitmapHeight;

	if (bytesPerPixel < 4)
	{
		unsigned char* expandedData = new unsigned char[width * height * 4];

		for (size_t y = 0; y < height; y++)
		{
			for (size_t x = 0; x < width; x++)
			{
				size_t pixelIndex = x + y * width;
				memcpy(&expandedData[pixelIndex * 4], &data[pixelIndex * bytesPerPixel], bytesPerPixel);
			}
		}

		delete[] data;
		data = expandedData;
	}

#ifndef NDEBUG
	std::stringstream ss;
	ss << "W:" << width << " H:" << height << " BPP:" << bytesPerPixel;
	Engine::DebugLog("Load", "Texture", filename, ss.str().c_str());
#endif
}

bool Model::LoadMesh(const char* filename, void*& vertexData, void*& normalData, void*& texcoordData, void*& colorData, size_t& vertexCount, void*& indexData, size_t& indexCount, bool generateNormals)
{
	objl::Loader meshLoader;
	if (!meshLoader.LoadFile(filename))
	{
		Engine::DebugError("Load", "Mesh", filename, "Couldn't load mesh file");
		return false;
	}

	vertexCount = meshLoader.LoadedVertices.size();
	vertexData = new float[vertexCount * 3];
	normalData = new float[vertexCount * 3];
	texcoordData = new float[vertexCount * 2];
	colorData = new float[vertexCount * 3];

	indexCount = meshLoader.LoadedIndices.size();
	indexData = new unsigned int[indexCount];
	memcpy(indexData, meshLoader.LoadedIndices.data(), sizeof(unsigned int) * indexCount);

	for (size_t i = 0; i < vertexCount; i++)
	{
		memcpy((float*)vertexData + i * 3, &meshLoader.LoadedVertices[i].Position, sizeof(float) * 3);
		memcpy((float*)normalData + i * 3, &meshLoader.LoadedVertices[i].Normal, sizeof(float) * 3);
		memcpy((float*)texcoordData + i * 2, &meshLoader.LoadedVertices[i].TextureCoordinate, sizeof(float) * 2);
	}

	size_t vertexOffset = 0;
	for (size_t i = 0; i < meshLoader.LoadedMeshes.size(); i++)
	{
		for (size_t j = 0; j < meshLoader.LoadedMeshes[i].Vertices.size(); j++, vertexOffset++)
		{
			memcpy((float*)colorData + vertexOffset * 3, &meshLoader.LoadedMeshes[i].MeshMaterial.Kd, sizeof(float) * 3);
		}
	}

	if (generateNormals)
	{
		normalData = GenerateNormals((float*)vertexData, vertexCount, (unsigned int*)indexData, indexCount);
	}

#ifndef NDEBUG
	std::stringstream ss;
	ss << "V:" << vertexCount << " I:" << indexCount;
	Engine::DebugLog("Load", "Mesh", filename, ss.str().c_str());
#endif

	return true;
}

size_t Engine::GetHeight()
{
	return swapChainDesc.BufferDesc.Height;
}

size_t Engine::GetWidth()
{
	return swapChainDesc.BufferDesc.Width;
}

Camera* Engine::GetCamera()
{
	return currentCamera;
}

Scene* Engine::GetScene()
{
	return currentScene;
}

Texture* Engine::GetRenderTarget()
{
	return currentRenderTexture;
}

float Engine::GetTime()
{
	return time;
}

float Engine::GetDeltaTime()
{
	return deltaTime;
}

void Engine::SetCamera(Camera* camera)
{
	currentCamera = camera;
	currentCamera->SetAspectRatio((float)GetWidth() / GetHeight());
}

void Engine::SetScene(Scene* scene)
{
	currentScene = scene;
}

void Engine::SetRenderTarget(Texture* renderTexture)
{
	currentRenderTexture = renderTexture;

	if (renderTexture)
	{
		deviceContext->OMSetRenderTargets(1, &currentRenderTexture->renderTargetView, currentRenderTexture->depthStencilView);
		deviceContext->RSSetViewports(1, &currentRenderTexture->viewport);
	}
	else
	{
		deviceContext->OMSetRenderTargets(1, &renderTarget, depthStencilView);
		deviceContext->RSSetViewports(1, &viewport);
	}
}

void Engine::UpdateTime()
{
	std::chrono::time_point<std::chrono::high_resolution_clock> lastTime =
		*((std::chrono::time_point<std::chrono::high_resolution_clock>*)lastTimePoint);

	std::chrono::time_point<std::chrono::high_resolution_clock> now =
		std::chrono::high_resolution_clock::now();

	deltaTime = std::chrono::duration_cast<std::chrono::duration<float>>(std::chrono::high_resolution_clock::now() - lastTime).count();

	time += deltaTime;

	*((std::chrono::time_point<std::chrono::high_resolution_clock>*)lastTimePoint) = now;
}

bool Engine::Run()
{
	while (PeekMessage(&message, NULL, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&message);
		DispatchMessage(&message);

		if (message.message == WM_QUIT) return false;
	}

	return true;
}

void Engine::Quit()
{
	SendMessage(hwnd, WM_CLOSE, NULL, NULL);
}

void Engine::Clear()
{
	ID3D11RenderTargetView* currentRenderTarget = nullptr;
	ID3D11DepthStencilView* currentDepthStencil = nullptr;
	deviceContext->OMGetRenderTargets(1, &currentRenderTarget, &currentDepthStencil);

	if(currentRenderTarget)
	{
		deviceContext->ClearRenderTargetView(currentRenderTarget, currentCamera->GetColor().vector4_f32);
	}

	if (currentDepthStencil)
	{
		deviceContext->ClearDepthStencilView(currentDepthStencil, D3D11_CLEAR_DEPTH, 1, 0);
	}
}

void Engine::GeometryPass()
{
	if (currentScene)
	{
		currentScene->Render();
	}
}

void Engine::Present()
{
	UpdateTime();
	swapChain->Present(vsync, 0);
}

void Engine::Render()
{
	Light::UpdateLights();

	Engine::SetRenderTarget();
	Engine::Clear();
	currentScene->Render();
	Engine::Present();
}

std::wstring Engine::ToWString(const char* text)
{
	size_t outLength = 0;
	size_t length = strlen(text);
	std::wstring wideText = std::wstring(length, L' ');
	mbstowcs_s(&outLength, &wideText[0], length + 1, text, length);
	return wideText;
}

void Engine::DebugMessage(const char* action, const char* subject, const char* context, const char* messageType, const char* message, const char* details)
{
	if (action != nullptr || subject != nullptr)
	{
		Engine::Debug("[", false);

		if (action != nullptr)
		{
			Engine::Debug(action, false);
		}

		if (action != nullptr && subject != nullptr)
		{
			Engine::Debug(" ", false);
		}

		if (subject != nullptr)
		{
			Engine::Debug(subject, false);
		}

		Engine::Debug("] ", false);
	}

	if (context != nullptr)
	{
		Engine::Debug(context, false);
	}

	if (context != nullptr && messageType != nullptr)
	{
		Engine::Debug(" - ", false);
	}

	if (messageType != nullptr)
	{
		Engine::Debug(messageType, false);
	}

	if (context != nullptr && message != nullptr || messageType != nullptr && message != nullptr)
	{
		Engine::Debug(" - ", false);
	}

	if (message != nullptr)
	{
		Engine::Debug(message, false);
	}

	if (details != nullptr)
	{
		Engine::Debug(": ", false);
		Engine::Debug(details, false);
	}
	else
	{
		Engine::Debug("");
	}
}

void Engine::DebugLog(const char* action, const char* subject, const char* context, const char* message, const char* details)
{
	DebugMessage(action, subject, context, "Info", message, details);
}

void Engine::DebugError(const char* action, const char* subject, const char* context, const char* message, const char* details)
{
	DebugMessage(action, subject, context, "Error", message, details);
}

DXGI_SWAP_CHAIN_DESC Engine::swapChainDesc;
IDXGISwapChain* Engine::swapChain;
ID3D11Texture2D* Engine::backBuffer;
ID3D11RenderTargetView* Engine::renderTarget;
ID3D11Texture2D* Engine::depthStencilBuffer;
ID3D11DepthStencilView* Engine::depthStencilView;
D3D11_VIEWPORT Engine::viewport;

Camera* Engine::currentCamera = nullptr;
Texture* Engine::currentRenderTexture = nullptr;
Scene* Engine::currentScene = nullptr;

float Engine::clearColor[4] = {0.1, 0.25, 0.6, 0.0};
bool Engine::vsync;

float Engine::time = 0;
float Engine::deltaTime = 0;
void* Engine::lastTimePoint = 0;

DirectX::XMFLOAT3 Camera::defaultColor = XMFLOAT3(0.1, 0.25, 0.6);

MSG Engine::message;

LRESULT CALLBACK Engine::wndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_KEYDOWN:
		if (wParam == VK_ESCAPE)
		{
			SendMessage(hWnd, WM_CLOSE, NULL, NULL);
		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, msg, wParam, lParam);
}

const float Light::DefaultRange = 10;
const float Light::DefaultIntensity = 1;
const float Light::DefaultAngle = 30;

std::vector<Light*> Light::lights;
ID3D11ShaderResourceView* Light::shadowMaps[];
LightData Light::renderableLights[];

void Light::UpdateLights()
{
	ZeroMemory(Light::renderableLights, sizeof(LightData) * MaxRenderableLightCount);
	ZeroMemory(shadowMaps, sizeof(ID3D11ShaderResourceView*) * MaxRenderableLightCount);
		
	if (lights.size() < MaxRenderableLightCount)
	{
		for (size_t i = 0; i < lights.size(); i++)
		{
			if (lights[i]->Metadata.type == Light::TYPE_SPOT && lights[i]->IsEnabled())
			{
				lights[i]->RenderShadowMap();
				shadowMaps[i] = (ID3D11ShaderResourceView*)lights[i]->shadowMap.GetView();
			}

 			memcpy(Light::renderableLights + i, lights[i], sizeof(LightData));
		}
	}
	else
	{
		// TODO: Improve light sorting and selection
		for (size_t i = 0; i < MaxRenderableLightCount; i++)
		{
			if (lights[i]->Metadata.type == Light::TYPE_SPOT && lights[i]->IsEnabled())
			{
				lights[i]->RenderShadowMap();
				shadowMaps[i] = (ID3D11ShaderResourceView*)lights[i]->shadowMap.GetView();
			}

			memcpy(Light::renderableLights + i, lights[i], sizeof(LightData));
		}
	}

	Engine::deviceContext->VSSetShaderResources(Material::TextureCount, MaxRenderableLightCount, shadowMaps);
	Engine::deviceContext->PSSetShaderResources(Material::TextureCount, MaxRenderableLightCount, shadowMaps);
	Engine::deviceContext->GSSetShaderResources(Material::TextureCount, MaxRenderableLightCount, shadowMaps);
	Engine::deviceContext->HSSetShaderResources(Material::TextureCount, MaxRenderableLightCount, shadowMaps);
	Engine::deviceContext->DSSetShaderResources(Material::TextureCount, MaxRenderableLightCount, shadowMaps);
}

void Light::RenderShadowMap()
{
	view = XMMatrixTranspose(lightView.GetViewMatrix());
	projection = XMMatrixTranspose(lightView.GetProjectionMatrix());

	Camera* prevCamera = Engine::GetCamera();
	Engine::SetCamera(&lightView);
	Engine::SetRenderTarget(&shadowMap);
	Engine::Clear();
	Engine::GeometryPass();
	Engine::SetRenderTarget();
	Engine::SetCamera(prevCamera);
}

#pragma region Materials
const std::string Material::DepthBufferShader;

const std::string Material::Unlit2DShader =
"Texture2D mainTexture : register(t0);"
"SamplerState mainSampler : register(s0);"

"struct VertexInput"
"{"
"	float4 position : POSITION;"
"	float2 tex : TEXCOORD;"
"};"

"struct PixelInput"
"{"
"	float4 position : SV_POSITION;"
"	float2 tex : TEXCOORD;"
"};"

"PixelInput mainVS(VertexInput input)"
"{"
"	PixelInput output;"

"	output.position = input.position;"
"	output.tex = input.tex;"

"	return output;"
"}"

"float4 mainPS(PixelInput input) : SV_TARGET"
"{"
"	return mainTexture.Sample(mainSampler, input.tex);"
"}";

const std::string Material::Unlit3DShader =
"cbuffer BatchBuffer : register(b12)"
"{"
"	matrix transform[1024];"
"};"

"cbuffer EngineBuffer : register(b13)"
"{"
"	matrix view;"
"	matrix projection;"
"};"

"Texture2D mainTexture : register(t0);"
"SamplerState mainSampler : register(s0);"

"struct VertexInput"
"{"
"	float4 position : POSITION;"
"	float2 tex : TEXCOORD;"
"};"

"struct PixelInput"
"{"
"	float4 position : SV_POSITION;"
"	float2 tex : TEXCOORD;"
"};"

"PixelInput mainVS(VertexInput input, uint id : SV_InstanceID)"
"{"
"	PixelInput output;"

"	output.position = mul(input.position, transform[id]);"
"	output.position = mul(output.position, view);"
"	output.position = mul(output.position, projection);"

"	output.tex = input.tex;"

"	return output;"
"}"

"float4 mainPS(PixelInput input) : SV_TARGET"
"{"
"	return mainTexture.Sample(mainSampler, input.tex);"
"}";
#pragma endregion Materials
