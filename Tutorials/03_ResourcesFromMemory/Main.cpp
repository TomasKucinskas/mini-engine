#include "Engine.h"

int main()
{
	Engine::Initialize("03_ResourceFromMemory", 1024, 768, false);

	Camera camera = Camera(0, 3, -6, 20);
	Engine::SetCamera(&camera);

	int textureData[64 * 64];
	for (size_t y = 0; y < 64; y++)
	{
		for (size_t x = 0; x < 64; x++)
		{
			bool isWhite = y % 2 ^ x % 2;

			if (isWhite)
			{
				textureData[y * 64 + x] = 0x00ffffff;
			}
			else
			{
				textureData[y * 64 + x] = 0x00000000;
			}
		}
	}

	Texture texture = Texture(64, 64, textureData);

	Material material = Material(Material::Unlit3DShader);
	material.SetTexture(0, &texture);

	float vertexData[] = {
		-1, 1, -1,
		-1, -1, -1,

		-1, 1, 1,
		-1, -1, 1,

		1, 1, 1,
		1, -1, 1,

		1, 1, -1,
		1, -1, -1,

		-1, 1, -1,
		-1, -1, -1,

		-1, 1, -1,
		1, 1, -1,

		1, -1, -1,
		-1, -1, -1,
	};

	float textureCoordData[] = {
		0, 0.3333f,
		0, 0.6666f,

		0.25f, 0.3334f,
		0.25f, 0.6666f,

		0.5f, 0.3334f,
		0.5f, 0.6666f,

		0.75f, 0.3334f,
		0.75f, 0.6666f,

		1, 0.3334f,
		1, 0.6666f,

		0.25f, 0,
		0.5f, 0,

		0.5f, 1,
		0.25f, 1,
	};

	unsigned int indexData[] = {
		0, 1, 2,
		2, 1, 3,

		2, 3, 4,
		3, 5, 4,

		4, 5, 6,
		6, 5, 7,

		6, 7, 8,
		7, 9, 8,

		2, 4, 10,
		4, 11, 10,

		3, 12, 5,
		3, 13, 12,
	};

	Model model;
	model.SetVertices(vertexData, 14);
	model.SetTexcoords(textureCoordData, 14);
	model.SetIndices(indexData, 36);
	model.SetMaterial(&material);
	model.SetRotation(0, 30, 0);

	while (Engine::Run())
	{
		Engine::Clear();
		model.Render();
		Engine::Present();
	}

	Engine::Deinitialize();
	return 0;
}