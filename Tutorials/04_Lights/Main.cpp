#include "Engine.h"

int main()
{
	Engine::Initialize("04_Lights", 1024, 768, false);

	Camera camera = Camera(0, 6, -10, 15);
	Engine::SetCamera(&camera);

	Texture texture = Texture("../../Resources/TestAssets/Liudas256.jpg");

	Material material = Material("../../Resources/Shaders/Standard.hlsl");
	material.SetTexture(0, &texture);

	Model model = Model("../../Resources/TestAssets/Liudapalme.obj");
	model.SetMaterial(&material);

	Model cube = Model("../../Resources/TestAssets/Cube.obj");
	cube.SetMaterial(&material);
	cube.SetScale(10, 1, 10);

	Light pointLight = Light::CreatePointLight(-3, 1, -3, 20, 0.25f, 0.75f, 2.0f);
	Light directionalLight = Light::CreateDirectionalLight(-45, 45, 0, 1, 0, 0, 0.5f);
	Light spotLight = Light::CreateSpotLight(-1.5, 12, 0, 30, 75, 135, 0, 0, 1, 0, 60);

	Scene scene;
	scene.Add(&model);
	scene.Add(&cube);
	Engine::SetScene(&scene);

	while (Engine::Run())
	{
		Engine::Render();
	}

	Engine::Deinitialize();
	return 0;
}