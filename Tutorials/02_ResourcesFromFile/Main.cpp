#include "Engine.h"

int main()
{
	Engine::Initialize("02_ResourceFromFile", 1024, 768, false);

	Camera camera = Camera(0, 6, -10, 15);
	Engine::SetCamera(&camera);

	Texture texture = Texture("../../Resources/TestAssets/Liudas256.jpg");

	Material material = Material("../../Resources/Shaders/Unlit3D.hlsl");
	material.SetTexture(0, &texture);

	Model model = Model("../../Resources/TestAssets/Liudapalme.obj");
	model.SetMaterial(&material);

	while (Engine::Run())
	{
		Engine::Clear();
		model.Render();
		Engine::Present();
	}

	Engine::Deinitialize();
	return 0;
}