#include "Engine.h"

int main()
{
	Engine::Initialize("05_Instancing", 1024, 768, false);

	Camera camera = Camera(-40, 10, -40, 15, 45, 0);
	Engine::SetCamera(&camera);

	Texture texture = Texture("../../Resources/TestAssets/Liudas256.jpg");

	Material material = Material(Material::Unlit3DShader);
	material.SetTexture(0, &texture);

	Model model = Model("../../Resources/TestAssets/Cube.obj");
	model.SetMaterial(&material);
	model.SetInstanceCount(1024);

	for (int z = 0; z < 32; z++)
	{
		for (int x = 0; x < 32; x++)
		{
			model.SetPosition(x * 2 - 32, 0, z * 2 - 32, z * 32 + x);
		}
	}

	while (Engine::Run())
	{
		Engine::Clear();
		model.Render();
		Engine::Present();
	}

	Engine::Deinitialize();
	return 0;
}